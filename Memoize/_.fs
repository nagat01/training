open FStap open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "Memoize"; version "0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "Memoize: __"; STAThread>] SET_ENG_CULTURE

// constants
let VisualStudio = [
  "Ctrl (Shift) -"   , "move cursor back / forward to" 
  "Ctrl (Shift) tab ", "switch open windows"
  "Alt Shift Enter"  , "maximize code window"
  "Ctrl l"           , "incremantal search"
  "F12"              , "goto defninition"
  "Shift F12"        , "find reference"
  "Ctrl Shift F"     , "find and replace"
  "Ctrl Space"       , "possible completions"
  "Ctrl F3"          , "search the current selection"
  "Ctrl k Ctrl k"    , "create the bookmark"
  "Ctrl k Ctrl n"    , "next bookmark" 
  ]

let VsVim = [
  "Ctrl + Tab:", "file selector popup"
  "q:"         , "close tab"
  ]

let Questions = ["Visual Studio", VisualStudio; "VsVim", VsVim]


// main
let wnd = Window' "Memoize"


// vars
let correctAll = pshi "correctAll" 0
let wrongAll   = pshi "wrongAll"   0


// ui
let sp            = StackPanel()     $ wnd 
let  dp           = DockPanel()     $ sp
let   _           = Label' "Question:" $ dp.left  $ fsz 20 $ h 100 $ bdr 2 "ddf" $ "eff".bg 
let   lblQuestion = Label'()           $ dp.right $ fsz 20 $ h 100 $ hcaCenter $ vcaCenter
let  ugBtns       = UGrid(c=2) $ sp
let   btnYes      = Button'("YES","dfd") $ ugBtns  $ fsz 20 $ "0f0".fg $ aCenterStretch $ hcaCenter
let   btnNo       = Button'("NO" ,"fdd") $ ugBtns  $ fsz 20 $ "f00".fg $ aCenterStretch $ hcaCenter
let  ugStats      = UGrid(c=2) $ sp
let   lblCorrect  = Label'()   $ ugStats $ fsz 20 $ "efe".bg $ aCenterStretch $ hcaCenter
let   lblWrong    = Label'()   $ ugStats $ fsz 20 $ "fee".bg $ aCenterStretch $ hcaCenter
let  lblAnswer    = Label'()   $ sp      $ fsz 20 $ h 100    $ aCenterLeft    $ vcaCenter


// events
immediate' {
for title, questions in Questions do
  for q, a in questions do
    lblQuestion <*- q
    let! result = btnYes -%% true >=< btnNo -%% false
    lblAnswer <*- sf "Question: %s\r\nAnswer  : %s" q a
    lblAnswer |> (result.If "0f0" "f00").fg
    if result then
      correctAll <*- correctAll +. 1
    else
      wrongAll <*- wrongAll +. 1
}

correctAll =+ { lblCorrect <*- sf "Correct All = %d" correctAll.v }
wrongAll   =+ { lblWrong <*- sf "Wrong All = %d" wrongAll.v }


// init
INIT_SHARED_VALUES
wnd.run