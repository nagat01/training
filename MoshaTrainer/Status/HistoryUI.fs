﻿[<AutoOpen>]
module Status.HistoryUI
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes

type HistoryBar(pa:StackPanel, day:s, software:i, active:i, canvas:i, drawing:i) =
  let durAll = 3600 * 4 * 1000 
  let wBar = 600.
  let bdr  = Border() $ pa $ bdr 0.5 "000"
  let grid = Grid() $ bdr 
  let sp   = HStackPanel() $ grid $ w wBar
  let ug   = UniformGrid'(c=5) $ grid $ w wBar
  let usages =
    [
      software, TimeCategory.Software
      active,   TimeCategory.Active
      canvas,   TimeCategory.Canvas
      drawing,  TimeCategory.Drawing
    ]
  do
    BorderedText(day, "fc0", 12., FontWeights.Bold, "000") 
    $ ug $ aTopLeft $ mgnw 2 |> ig
    for dur, cate in usages.rev do
      let co = cate.color.str 
      let _w = wBar *. dur /. durAll
      Rectangle() $ sp $ wh _w 20 $ fill co |> ig
      BorderedText(dur.ms.sJapanese, co, 10., FontWeights.UltraBold, "000") 
      $ ug $ cate.ha $ vaBottom $ mgnw 2 |> ig

let bdrHistory = Border() $ bdr 0.5 "000" $ w 601.
let spHistory = StackPanel() $ bdrHistory

let ini (add:Border -> _) =
  bdrHistory |> add |> ig
  spHistory |>* bdrHistory
  for history in prefTenDays do
    HistoryBar(
      spHistory,
      history.file.nameNoExt,
      history.i "sumSoftware" |? 0,
      history.i "sumActive"   |? 0,
      history.i "sumCanvas"   |? 0,
      history.i "sumDrawing"  |? 0).ig 