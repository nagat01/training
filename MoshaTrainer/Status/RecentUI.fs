﻿[<AutoOpen>]
module Status.RecentUI
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Collections.Generic


type RecentBar(minutes:i, annots:s) as __ =
  inherit Grid()
  let annots = annots.words
  let vline  = 600 / (annots.len - 1)
  let img = Image() $ __
  let  wb = mkWriteableBitmap 600 40 $ img
  let cnv = Canvas() $ __
  let _   = BorderedText(sf "%sの記録" annots.last, "000", 12.) $ __ $ aTopLeft
  let pace (__:TimeCategoryUsage) = 
    __.usage * (__.category.mat -2 -1 1 2) /. 10000.
  do
    __ |> wh 600 40
    for i, s in annots.seqi do
      BorderedText(s, "000", 9.) $ cnv.addTo(po (i*vline-10) 30) |> ig
    update >< panelState -? { V (panelState =. PanelState.Status) } =+ {
      let values = ra()
      let mutable paceHyst = 0.5
      let prev = None'
      let tcs = qRecent +++ prevTimeCategory
      let skipped = tcs.sumBy(fun __ -> __.usage.i64) - minutes.i64 * 60000L
      let mutable acc = 0L
      let q = 
        Queue(
          tcs |> Seq.skipWhile(fun __ -> 
            acc <- acc + __.usage.i64
            acc < skipped ) )
      
      let next() = q.Count > 0 || prev.ok
      while next() do
        let cates = ra()
        let mutable total = 0
        while total < minutes * 100 && next() do
          let cate = if prev.ok then prev.v else q.deq
          total <- total + cate.usage
          let over = total - minutes * 100
          if over > 0 then
            cates <*- { category = cate.category; usage = cate.usage - over }
            prev <*- { category = cate.category; usage = over }
          else
            cates <*- cate
            prev.none
        let co   = colorOfMany cates
        let pace = ( cates |%> pace |> Seq.sum ) /. minutes
        let pace = pace - (paceHyst - 0.5) * 0.03
        paceHyst <- within 0. 1. (paceHyst + pace)
        values <+ (co, paceHyst)

      let values = values.array.rev
      wb.writeRectXY wb.r32 <| fun x y ->
        let co =
          if x % vline = 0 && (x + y) % 2 = 0 then
            "444".co
          elif (x % 30 = 0 || y % 10 = 0) && (x + y) % 2 = 0 then
            "aaa".co
          elif x < values.len then
            let co, pace = values.[x]
            let d = abs (39 - y -. pace * 39.)
            if d < 1. then 
              let a = 255. * (1. - d) |> int
              let v, w = y % 8, 8 - y % 8
              let r,g,b =
                match y with
                | LT 8  -> w, 8, 0
                | LT 16 -> 0, 8, v
                | LT 24 -> 0, w, 8
                | LT 32 -> v, 0, 8
                | _     -> 8, 0, w
              let f x = x * 255 / 8
              blend co (ofArgb a (f r) (f g) (f b)) 
            else co
          else 
            "fff".co
        co.i }

let  ugRecent = UniformGrid'(r=3) 

let ini (add:Panel -> _) =
  ugRecent |> add |> ig
  for minutes,annots in [5,"0分 2分30秒 5分"; 30,"0分 10分 20分 30分"; 180,"0分 1時間 2時間 3時間"] do
    RecentBar(minutes, annots) $ ugRecent $ mgnb 4 |> ig

  for s in (pref ? recent |? "").chunk 8 do
    let i = i.Parse(s, Globalization.NumberStyles.HexNumber)
    let cate = (i >>> 30) &&& 3 |> TimeCategory.ofI
    let ms = i &&& 0x3fffffff
    qRecent.enq { category = cate; usage = ms } 

  wnd.Closing =+ {
  truncateQRecent
  pref.["recent"] <- String.Concat(qRecent |%> fun __ -> sf "%08x" ((__.category.toI <<< 30) ||| __.usage) )
  }
