﻿[<AutoOpen>]
module Status.ProfileUI
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes
      

type ProfileBar(wAll:f, spPa:StackPanel, ugPa:UniformGrid', cate:TimeCategory) =
  let desc = cate.desc
  let co   = cate.color.str
  let rect     = Rectangle() $ spPa $ fill co $ h 25.
  let sp       = HStackPanel() $ ugPa $ cate.ha $ mgnt 1
  let  _       = BorderedText(desc, "000", 10.)         $ sp $ mgnw 3 
  let  lblTime = BorderedText("", co, 16., bdrco="000") $ sp $ mgnw 3 
  do
    update =+ { 
      let all = TimeCategory.Software.timeSum
      let ts = cate.time
      let s = ts.ms.sJapanese
      if all <> 0 then
        let w = wAll * (ts /. all)
        rect.w <- w
      lblTime <*- s }

let bdrProfile   = Border()  
let  gridProfile = Grid()     
let   spProfile  = HStackPanel() 
let   ugProfile  = UniformGrid'(c=4) 

let ini (add:Border -> _) =
  bdrProfile |> add |> ig
  gridProfile |>* bdrProfile
  spProfile |>* gridProfile
  ugProfile |>* gridProfile
  for category in TimeCategory.all.rev do
    ProfileBar(600., spProfile, ugProfile, category).ig

