﻿[<AutoOpen>]
module Status.StampUI
open FStap open __ open System.Windows.Controls
open System.Windows.Shapes

let maStamp = 100

type StampBar(title, fgco:s, bgco:s, ratio, ?isLeft) as __ =
  inherit Grid()
  let ha = If (isLeft |? false) haLeft haRight
  let _ = Rectangle()  $ __ $ haLeft $ wh (ratio * 70.5) 17.75 $ fill bgco
  let _ = Label' title $ __ $ ha     $ fsz 12 $ fgco.fg


type StampPanel(limit:f, elapsed:f, timeCanvas:f, timeDrawing:f) as __ =
  inherit Border()
  let time (time:f) = time.sec.sJapanese
  let limit      = limit.sec.sJapanese
  let ratio time = sqrt( time / elapsed )

  let ug = UniformGrid'(r=4) $ __ $ "fff".bg 
  let  _ = StampBar(limit,            "000", "fff", 1.,isLeft=true) $ ug
  let  _ = StampBar(time elapsed,     "777", "ddd", ratio elapsed) $ ug
  let  _ = StampBar(time timeCanvas,  "44f", "ddf", ratio timeCanvas) $ ug
  let  _ = StampBar(time timeDrawing, "0c0", "dfd", ratio timeDrawing) $ ug
  do __ $ size1 71.5 $ mgn4 4 4 0 0 $ bdr 0.25 "000" |> ig      

let scrStamp = ScrollViewer() 
let wpStamp = WrapPanel() 
 
let addStamp a b c d =
  qStamp.enqTruncate (a, b, c, d) 100
  if wpStamp.Children.Count >= 100 then
    wpStamp.Children.RemoveAt 99
  StampPanel(a, b, c, d) |> wpStamp.addHead

let reset<'__> =
  prevStamp   <- now 
  prevCanvas  <- sumCanvas.v 
  prevDrawing <- sumDrawing.v

let ini(add:_ -> _) =
  scrStamp |> add |> ig
  wpStamp |>* scrStamp
  seq { for i in 0..99 -> prefToday ? (sf "s%d" i) } 
  |> Seq.takeWhile Option.isSome
  |%| iter(fun (stamp:s) -> try' {
    let fs = stamp.split "(,)".ss
    let f i = f.Parse fs.[i]
    addStamp (f 0) (f 1) (f 2) (f 3) })

  stamped =+ {
  let limit       = Stopwatch.limit.v
  let elapsed     = (nowM &prevStamp).tsec
  let paceCanvas  = (sumCanvas  -. prevCanvas) /. 1000.
  let paceDrawing = (sumDrawing -. prevDrawing) /. 1000.
  addStamp limit elapsed paceCanvas paceDrawing
  reset }

  imgPath -? { V imgReset } =+ { reset }

  wnd.Closing =+ {
  for i, stamp in qStamp.seqi do
    prefToday.[sf "s%d" i] <- sf "%A" stamp }

