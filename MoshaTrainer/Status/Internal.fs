﻿[<AutoOpen>]
module Status.Internal
open FStap open __ open System          
open System.Collections.Generic
open Pref

[<RequireQualifiedAccess>]
type TimeCategory = 
  Software | Active | Canvas | Drawing
  member __.mat a b c d = 
    match __ with
    | TimeCategory.Software -> a
    | TimeCategory.Active -> b
    | TimeCategory.Canvas -> c
    | TimeCategory.Drawing -> d
      
  static member all = [Software; Active; Canvas; Drawing]
  static member _color (__:TimeCategory) = 
    __.mat
      (0xff, 0xdf, 0xdf)
      (0xe7, 0xf7, 0xff)
      (0, 0xdf, 0xff)
      (0, 0xff, 0)
  
  member __.color = 
    TimeCategory._color __ |||> ofRgb
  member __.ha = __.mat haRight haCenter haCenter haLeft

  member __.toI       = __.mat 0 1 2 3
  static member ofI i = match i with | 0 -> Software | 1 -> Active | 2 -> Canvas | _ -> Drawing
       
type TimeCategoryUsage = { mutable category:TimeCategory; mutable usage:i }

let colorOfMany (__:TimeCategoryUsage seq) =
  let rsum, gsum, bsum, msum = ref 0L, ref 0L, ref 0L, ref 0L
  for { category = timeCategory; usage = m } in __ do
    let r, g, b = TimeCategory._color timeCategory
    rsum := !rsum + (r * m).i64
    gsum := !gsum + (g * m).i64
    bsum := !bsum + (b * m).i64
    msum := !msum + m.i64
  let ave sum = int(!sum / !msum)
  if !msum = 0L then
    "fff".co
  else
    ofRgb (ave rsum) (ave gsum) (ave bsum)


// time
let tm100msec = mkTimer 100

let update = mkTimer 1000 -? { V (panelState =. PanelState.Status) }

let today    = now.AddHours(-3.).Date
let sToday   = today.S "MM'月'dd'日'"
let keyToday = today.S "yyyy年MM月dd日"

let dirHistory = pwd +/ Folders.history

// file
dirHistory.mkDir
let prefToday = 
  Pref(Folders.history +/ (keyToday), wnd.Closing.wait)
let fToday k d = prefToday.shF k d
let iToday k d = prefToday.shI k d
let prefTenDays = Seq.truncate 10 <| seq { 
  for i in 1 .. 100 do
    let file = dirHistory +/ ( today.AddDays(- !.i).S "yyyy年MM月dd日")
    if file.ok then 
      let pref = Pref(file, isSave = false)
      if (pref.i "sumDrawing").ok then
        yield pref }


// status
let sumPoint    = fToday "sumPoint"    0.
let sumSoftware = iToday "sumSoftware" 0
let sumActive   = iToday "sumActive"   0
let sumCanvas   = iToday "sumCanvas"   0
let sumDrawing  = iToday "sumDrawing"  0
let mutable qRecent = Queue<TimeCategoryUsage>()

let mutable prevTimeCategory = { TimeCategoryUsage.category = TimeCategory.Software; usage = 0 }
let mutable tmPrevTimeCategory = now 
let mutable prevStamp   = now
let mutable prevCanvas  = sumCanvas.v
let mutable prevDrawing = sumDrawing.v
let qStamp = Queue<f*f*f*f>()


// funcs
type TimeCategory with
  member __.time = (__.mat sumSoftware sumActive sumCanvas sumDrawing).v 
  member __.timeSum = 
    __.mat
      (sumSoftware +. sumActive +. sumCanvas +. sumDrawing)
      (sumActive +. sumCanvas +. sumDrawing)
      (sumCanvas +. sumDrawing)
      sumDrawing.v
  member __.desc = __.mat "起動" "操作" "移動" "描画"


let truncateQRecent<'__> =
  let sum = qRecent.sumBy(fun __ -> __.usage.i64)
  let acc = ref 0L
  qRecent <- Queue( qRecent |> Seq.skipWhile(fun __ -> 
    acc := !acc + __.usage.i64
    !acc + 10800000L < sum ) )
 

sumPoint =>* State.sumPoint

