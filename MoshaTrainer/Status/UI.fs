﻿module Status.UI
open FStap open __ open System.Windows.Controls
open Status.Internal

let dp = DockPanel() $ "cfff".bg 

let ini (pa:Panel) (icnv:InkCanvas) () =
  dp $ lastChildFill $ collapsed |>* pa
  RecentUI.ini(fun __ -> __ $ wh 600 132 $ mgnt 4 $ dp.top)
  ProfileUI.ini(fun __ -> __ $ wh 600 20 $ bdr 0.25 "000" $ dp.top)
  HistoryUI.ini(fun __ -> __ $ mgnb 4 $ dp.bottom)
  StampUI.ini(fun __ -> __ $ w 608 $ mgnt 4 |>* dp.top)

  let mutable cntTruncate = 0
  tm100msec =+ {
  let ts = (now - tmPrevTimeCategory).tmsec.i
  tmPrevTimeCategory <- now
  let add enum (time:i sh) = 
    time <*- time +. ts
    if prevTimeCategory.category = enum then
      prevTimeCategory.usage <- prevTimeCategory.usage + ts
    else
      qRecent.enq prevTimeCategory
      if &cntTruncate +=! 1 > 100 then
        truncateQRecent
      prevTimeCategory <- { category = enum; usage = ts; }
      
  if isMouseDown then 
    add TimeCategory.Drawing sumDrawing
    sumPoint <*- sumPoint +. (ts /. 100.)
  elif icnv.IsMouseOver then 
    add TimeCategory.Canvas sumCanvas 
  elif wnd.IsMouseOver then 
    add TimeCategory.Active sumActive 
  else 
    add TimeCategory.Software sumSoftware 
  }

