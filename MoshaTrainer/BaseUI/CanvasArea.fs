﻿[<AutoOpen>]
module __.CanvasArea
open FStap open __ open System open System.Windows.Controls
open System.Windows.Input
open System.Windows.Media
open System.Windows.Shapes
open Shortcut


let tipImage _ =  
  sf "右ドラッグ: 画像を平行移動(%s,%s,%s,%s) 左ドラッグ:画像の拡大縮小(%s,%s)と回転(%s,%s)" 
    imageLeft.sKey imageBottom.sKey imageTop.sKey imageRight.sKey 
    zoomIn.sKey zoomOut.sKey rotateLeft.sKey rotateRight.sKey


let lblUgrid     = Label'()  $ spUgrid 
let  ugrid       = UniformGrid' 2   $ lblUgrid $ "f777".bg 
let   grid0      = Grid()    $ ugrid    $ clipToBounds 
let    gridImg       = Grid()   $ grid0    $ aCenter $ tip "マウスホイールを、下に回すと次の画像、上に回すと前の画像になります。"
let     gridImg2     = Grid()   $ gridImg  $ "fff".bg
let      imgBlurred  = Image()  $ gridImg2 
let      gridImg3    = Grid()   $ gridImg2
let       _          = imgImage $ gridImg3 $ tip'' tipImage 
let       imgOutline = Image()  $ gridImg3
let      deskelRel1  = Grid()   $ gridImg2 $ nohit
let     deskel1  = Grid()    $ grid0 $ nohit
let    sep0      = Line()    $ grid0    $ haRight $ xy2 0 -999 0 9999 $ stroke 1 "0f0" 
let   grid1      = Grid()    $ ugrid    $ clipToBounds
let    gridCnv   = Grid()    $ grid1    $ aCenter $ tip' { yield "キャンバス: " + ( match Stopwatch.state.v with StopwatchState.Correcting -> "マウスホイールを下に回す or Gキー で次の画像になります。" | _ -> "ここに模写してみてください。" ) }
let     gridCnv2 = Grid()    $ gridCnv  $ "fff".bg
let      lblCnv  = Label'()  $ gridCnv2 $ aCenter $ "4fff".bg
let      imgCnv  = Image()   $ gridCnv2 
let      bdrCnv  = Border()  $ gridCnv2 $ aCenter $ bdr 1 "40f0" $ nohit 
let      icnv  = InkCanvas() $ gridCnv2 $ aCenter $ "0fff".bg $ cursor Cursors.Cross
let      deskelRel2 = Grid() $ gridCnv2 $ nohit
let    deskel2   = Grid()    $ grid1 $ nohit 
let    sep1      = Line()    $ grid1    $ haRight $ xy2 0 -999 0 9999 $ stroke 1 "0f0"  

let setUGrid<'__> =
  let isSingle = isSeparate.v || isCanvas.v.n
  spUgrid.clear 
  ugrid.clear
  spSepUGrid.clear

  if isSingle 
  then lblUgrid |>* spUgrid    
  else If isLeft.v (uies[lblUgrid; spPalette]) (uies[spPalette; lblUgrid]) |%|* spUgrid

  ugrid.Columns <- isSingle.If 1 2
  if isSeparate.v then
    grid0 |>* ugrid
    If isLeft.v (uies[grid1; spPalette]) (uies[spPalette; grid1]) |%|* spSepUGrid
  else
    If isLeft.v [grid0; grid1] [grid1; grid0] |%|* ugrid

let trSc     = ScaleTransform() 
let trMouse  = MouseTransformer gridImg2
let trScFlip = ScaleTransform()

do
  let gs  = trMouse.group.Children
  let trs = trSc :> Transform :: gs.list @ [trScFlip]
  gs.Clear()
  trs |%| gs.Add
  let tg = TransformGroup()
  trs |%| tg.add
  gridCnv2.rtr <- tg


RenderOptions.SetBitmapScalingMode (imgImage, BitmapScalingMode.HighQuality)
               

let layout<'__> = try' {
  do! imgImage.Source.ok
  let nc  = ugrid.Columns.f
  let a   = trMouse.angle / 180. * Math.PI
  let sc  = trMouse.sc'
  let rsc = 1. / sc
  let sz  = szImage.v
  let h   = ugrid.rh * sc
  let _w  = sz.w / sz.h * h
  let w0  = abs(_w * cos a) + abs(h * sin a)
  let w1  = wGridMain / nc
  let w   = min w0 w1
  ugrid.w <- w * nc
  grid1.w <- if isSeparate.v then ugrid.w else ugrid.w / 2.
  grid1.h <- ugrid.rh
  gridImg.h <- h
  gridImg.w <- _w
  gridCnv.h <- h
  gridCnv.w <- _w
  let sz = imgImage.rsz
  let center = sz /. 2.
  trSc.set rsc center
  sz.po /. 2. |> trScFlip.center
  gridCnv2.Sz sz
  bdrCnv.Sz sz
  lblCnv.rtr <- mkScaleTransform 1.2 (sz /. 2. *. 1.2)
  lblCnv.Sz (sz *. 1.2)
  icnv.rtr <- mkScaleTransform (2. * sc) (sz /. 2. /. sc)
  icnv.Sz (sz /. sc) }
 
let ugridUpdated = gridMain.sized >< trMouse >< imgPath >< isLeft >< isCanvas

ugridUpdated =+! {
  layout
  do! 10
  layout }

isFliped => fun __ ->
  trScFlip.ScaleX <- __.If -1. 1.
  trMouse.isFliped <- __
