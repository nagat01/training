﻿[<AutoOpen>]
module __.MainPanel
open FStap open __ open System.Windows.Controls


let dp            = DockPanel()  
let  gridMenu     = Grid() $ haStretch $ zIdx 1 
let   spCnvTop    = HStackPanel() $ "fff".bg
let   spImg       = HStackPanel() $ haLeft $ "fff".bg
let  dpBar        = DockPanel() $ zIdx 1
let   spCnvBottom = HStackPanel() $ "fff".bg
let   gridGage    = Grid() $ "fff".bg
let  gridMain     = Grid()
let   spUgrid     = HStackPanel() $ haCenter
let   spPalette   = StackPanel() $ vaCenter $ "000".bg

let _            = dpSep $ lastChildFill $ "777".bg
let  gridSepMenu = Grid() $ dpSep.top $ haStretch $ zIdx 1 
let  dpSepBar    = DockPanel() $ dpSep.bottom $ zIdx 1 
let  gridSepMain = Grid() $ dpSep $ aCenterCenter
let   spSepUGrid = HStackPanel() $ gridSepMain

let setMenu<'__> =
  gridMenu.clear
  gridSepMenu.clear
  spCnvTop |>* isSeparate.v.If gridSepMenu gridMenu
  spImg |>* gridMenu  

let setBar<'__> =
  dpBar.clear
  dpSepBar.clear
  spCnvBottom |>* isSeparate.v.If dpSepBar dpBar
  gridGage |>* dpBar     

let ini(pa:Panel)() =
  dp |>* pa
  gridMenu |>* dp.top 
  setMenu
  dpBar    |>* dp.bottom
  setBar
  gridMain |>* dp.bottom 
  spUgrid  |>* gridMain


let wGridMain<'__> = max 1. (gridMain.rw - If (isSeparate.v.n && isCanvas.v) spPalette.rw 0.)

