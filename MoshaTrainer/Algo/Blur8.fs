﻿module Algo.Blur8
open FStap 

let inline conv (bs:i[]) d o = 
  let v = bs.[o] + bs.[o + d]
  bs.[o] <- v
  bs.[o + d] <- v

let inline mul (bs:i[]) i =
  bs.[i] <- bs.[i] <<< 1

type Blur(w, h, ps:i[], wait:i, idBlur:i ref, id) =
  let bs:i[] = Array.zeroCreate (w * h)
  let l = w * h - 1
  do
    for y in 0 .. h-1 do
      let o = y*w
      for x in 0 .. w/4-1 do 
        let a, r, g, b = iargb ps.[o + x]
        let i = o + x*4
        bs.[i] <- b
        bs.[i + 1] <- g
        bs.[i + 2] <- r
        bs.[i + 3] <- a
 
  let mulX x = for y in 0 .. h - 1 do mul bs (y * w + x)
  let mulY y = for x in 0 .. w - 1 do mul bs (y * w + x)
 
  let cancel body = async {
    do! wait
    do! (id = !idBlur)
    body() } 

  let cross ox oy = cancel <| fun _ ->
    for y in oy .. 2 .. h - 2 do
      for x in ox .. 2 .. w - 2 do
        let o = y * w + x
        conv bs (w+1) o 
        conv bs (w-1) (o+1) 
    if ox = 1           then mulX 0
    if (w - ox) % 2 = 1 then mulX (w - 1)
    if oy = 1           then mulY 0
    if (h - oy) % 2 = 1 then mulY (h - 1)

  let cross4 = async {
    do! cross 0 0
    do! cross 1 0
    do! cross 1 1
    do! cross 0 1 }

  let lineH ox = cancel <| fun _ ->
    for x in ox .. 2 .. w - 2 do
      for y in 0 .. h - 1 do
        conv bs 1 (y * w + x)
    if ox = 1           then mulX 0
    if (w - ox) % 2 = 1 then mulX (w - 1)
 
  let lineV oy = cancel <| fun _ ->
    for y in oy .. 2 .. h - 2 do
      for x in 0 .. w - 1 do
        conv bs w (y * w + x)
    if oy = 1           then mulY 0
    if (h - oy) % 2 = 1 then mulY (h - 1)
 
  let line = async {
    do! lineH 0
    do! lineV 0
    do! lineH 1
    do! lineV 1 }

  let lineNH n ox = cancel <| fun _ ->
    let d = n + 1
    for x in ox .. 2 .. w - 1 - d do
      for y in 0 .. h - 1 do
        let o = y * w + x
        conv bs d o
    for x in ox ^^^ 1 .. 2 .. d - 1 do mulX x
    let mi = w - d
    let mi = mi + (mi &&& 1) ^^^ ox
    for x in mi .. 2 .. w - 1 do mulX x
 
  let lineNW n oy = cancel <| fun _ ->
    let d = n + 1
    let dw = d * w
    for y in oy .. 2 .. h - 1 - d do
      for x in 0 .. w - 1 do
        let o = y * w + x
        conv bs dw o
    for y in oy ^^^ 1 .. 2 .. d - 1 do mulY y
    let mi = h - d
    let mi = mi + (mi &&& 1) ^^^ oy
    for y in mi .. 2 .. h - 1 do mulY y

  let lineN n = async {
    do! lineNW n 0
    do! lineNH n 0
    do! lineNW n 1
    do! lineNH n 1 }

  let bs_ps = cancel <| fun _ ->
    for i in 0 .. l do
      ps.[i] <- 0xff000000 + 0x10101 * (bs.[i] >>> 24)
 

  member __.calc = async {
    do! lineN 32
    do! lineN 16
    do! lineN 8
    do! lineN 4
    do! line 
    do! cross4
    do! bs_ps
    return ps
    }
