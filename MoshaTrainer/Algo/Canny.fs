﻿module Algo.Canny
open FStap
open System.Collections.Generic

let bpp_8_32 w h (ps:i[]) =
  let res = Array.zeroCreate(w*h)
  for y in 0 .. h-1 do
    let o = y*w
    for x in 0 .. w/4-1 do
      let a, r, g, b = iargb ps.[o+x]
      let i = o + x * 4
      let inline f d v = res.[i + d] <- 0xff000000 + v * 0x10101
      f 0 b
      f 1 g
      f 2 r
      f 3 a
  res

let filter (idCanny:i ref) id sth wth w h (ps:i[]) = 
  let is() = Array.zeroCreate (w*h)
  let bs() = Array.zeroCreate (w*h)

  let l = w*h-1
  let gauss = is()
  let weak = HashSet() 
  let strong = Queue()
  let visit, isWeak = bs(), bs()
  let gx, gy, sx, sy = is(), is(), is(), is()
  let canny = HashSet()
  let dark = HashSet()
  let res = is()

  let cancel body = async {
    do! 10
    do! (id = !idCanny)
    body() }

  let gausiaan = async {
    let r, g, b = psToRGB ps w h
    let inline add3 d s =
      let inline add (bs:i[]) = bs.[d] <- bs.[d] + bs.[s]
      add r 
      add g 
      add b
    let add4 = cancel <| fun _ ->
      for y in 0 .. h-2 do
        for i in y*w .. y*w + w-1 do
          add3 i (i+w)
      for y in h-1 .. -1 .. 1 do
        for i in y*w .. y*w + w-1 do
          add3 i (i-w) 
      for y in 0 .. h-1 do
        for i in y*w .. y*w + w-2 do
          add3 i (i+1) 
      for y in 0 .. h-1 do
        for i in y*w + w-1 .. -1 .. y*w + 1 do
          add3 i (i-1) 
    let render = cancel <| fun _ ->
      for i in 0 .. l do
        gauss.[i] <- 0xff000000 
          + ((r.[i] <<< 8) &&& 0xff0000)
          + ((g.[i]      ) &&& 0xff00)
          + ((b.[i] >>> 8) &&& 0xff) 

    do! add4
    do! add4
    do! render }

  let gradients = cancel <| fun _ ->
    let yr, yg, yb = is(), is(), is()
    let xr, xg, xb = is(), is(), is()
    for y in 1 .. h-2 do
      for i in y*w + 1 .. y*w + w-2 do
        let inline conv (ay:i[]) (ax:i[]) b =
          ax.[i + w] <- b
          ax.[i] <- ax.[i] + b * 2
          ax.[i - w] <- ax.[i - w] + b
          ay.[i + 1] <- b
          ay.[i] <- ay.[i] + b * 2
          ay.[i - 1] <- ay.[i - 1] + b
        let _, r, g, b = iargb ps.[i]
        conv yr xr r
        conv yg xg g
        conv yb xb b

    for y in 1 .. h-2 do
      for i in y*w + 1 .. y*w + w-2 do
        let inline f (a:i[]) = pown (a.[i+1] - a.[i-1]) 2
        let xi = f xr + f xg + f xb
        let inline f (a:i[]) = pown (a.[i+w] - a.[i-w]) 2
        let yi = f yr + f yg + f yb
        if xi + yi > wth then
          weak <+ i
          gx.[i] <- xi
          gy.[i] <- yi
          let inline fx (ax:i[]) = ax.[i+1] - ax.[i-1]
          let inline fy (ay:i[]) = ay.[i+w] - ay.[i-w]
          sx.[i] <- fx xr + fx xg + fx xb
          sy.[i] <- fy yr + fy yg + fy yb

  let direction i =
    let x = gx.[i]
    let y = gy.[i]
    let dx = if sx.[i] > 0 then 1 else -1
    let dy = if sy.[i] > 0 then w else -w
    if x = 0 then w else
      let t = (408*y) / (169*x) 
      if t >= 1 then
        let t = (169*y) / (408*x) 
        if t >= 1 then dy else dx+dy
      else dx
 
  let suppression = cancel <| fun _ ->
    for i in weak do
      let d = direction i
      let g = gx.[i] + gy.[i]
      let g2 = gx.[i+d] + gy.[i+d] 
      let g3 = gx.[i-d] + gy.[i-d]
      if g > g2 && g > g3 then
        isWeak.[i-d] <- true
        if g > sth then
          strong.enq (i-d)

  let hysteresis = cancel <| fun _ ->
    while strong.Count > 0 do 
      let i = strong.deq
      let x = i % w
      let y = i / w
      let inline append i =
        if isWeak.[i] && not visit.[i] then
          canny <+ i
          strong.enq i
          visit.[i] <- true      
      if x > 0   then append (i-1)
      if x < w-1 then append (i+1)
      if y > 0 then
        append (i-w)
        if x > 0   then append (i-w-1)
        if x < w-1 then append (i-w+1)
      if y < h-1 then
        append (i+w)
        if x > 0   then append (i+w-1)
        if x < w-1 then append (i+w+1)

  let darkness = cancel <| fun _->
    let inline br i = let _, r, g, b = iargb ps.[i] in max3 r g b
    for i in weak do
      let d = direction i
      let b = br (i-d)
      if b < 128 then
        dark <+ (i-d)

  let render = cancel <| fun _ ->
    for i in dark do
      res.[i] <- res.[i] + 0x7f000000 
    for i in canny do
      res.[i] <- 0xff000000 

  async {
  do! gausiaan 
  do! gradients
  do! suppression
  do! hysteresis
  do! darkness
  do! render
  return res }

