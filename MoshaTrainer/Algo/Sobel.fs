﻿module Algo.Sobel
open FStap

let inline conv w (arrY:i[]) (arrX:i[]) p p0 p1 p2  =
  let y = p0 + p1 * 2 + p2
  arrY.[p-w] <- arrY.[p-w] + y
  arrY.[p+w] <- -y
  let x = -p0 + p2
  arrX.[p-w] <- arrX.[p-w] + x
  arrX.[p]   <- arrX.[p] + x * 2
  arrX.[p+w] <- x
 

let sobel threshold w h (pxs:i[]) =
  let mk() :i[] = Array.zeroCreate (w * h)
  let arrYR, arrYG, arrYB = mk(), mk(), mk()
  let arrXR, arrXG, arrXB = mk(), mk(), mk()
  for y in 1 .. h-2 do
    let offset = y * w
    for p in 1 + offset .. w - 2 + offset do
      let _, p0r, p0g, p0b = iargb pxs.[p-1]
      let _, p1r, p1g, p1b = iargb pxs.[p]
      let _, p2r, p2g, p2b = iargb pxs.[p+1]
      conv w arrYR arrXR p p0r p1r p2r
      conv w arrYG arrXG p p0g p1g p2g
      conv w arrYB arrXB p p0b p1b p2b
  let arr = mk()
  for y in 2 .. h-3 do
    let offset = y * w
    for p in 1 + offset .. w - 2 + offset do
      let g =
        pown arrYR.[p] 2 + pown arrYG.[p] 2 + pown arrYB.[p] 2 +
        pown arrXR.[p] 2 + pown arrXG.[p] 2 + pown arrXB.[p] 2
      arr.[p] <- if g > threshold then 0xff000000 else 0
  arr

