﻿[<AutoOpen>]
module Algo.Core
open FStap

let psToRGB (ps:i[]) w h =
  let mkArray() :i[] = Array.zeroCreate (w * h)
  let r = mkArray()
  let g = mkArray()
  let b = mkArray()
  for i in 0 .. w * h - 1 do
    let _, _r, _g, _b = iargb ps.[i]
    r.[i] <- _r
    g.[i] <- _g
    b.[i] <- _b
  r, g, b

 