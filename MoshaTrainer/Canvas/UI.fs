﻿module Canvas.UI
open FStap open __ open System.Windows.Controls

type Shortcut = Shortcut.Shortcut

let tools = ra()


let tool (sc:Shortcut) ico text = IconButton (ico, shortcut = sc, coKey="8af", text=text, co="fff", coon="dfd") $ tools
let btn  (sc:Shortcut) ico text = IconButton (ico, shortcut = sc, coKey="8af", text=text, co="fff")  


let checkButton (pa:Panel) =
  let cnv     = Canvas() $ pa
  let  btn    = IconButton (Bitmap.check, shortcut=Shortcut.check, coKey="8af", text="チェック", co="fff", opaoff=0.25)  
  let  sp     = StackPanel() $ composeMenuTopIf (fun _ -> swDraw.isMSec 100) cnv btn $ "ddd".bg
  let   spBar = HStackPanel() $ sp 
  let    _    = Label' "重ね合わせる\r\n画像の不透明度" $ spBar $ fsz 7 $ "fff".bg
  let    sli  = GageSlider("answerImageOpacity", 0.5, 50., "ddd", "777", "777") $ spBar
  let showCheck b =
    btn <*- b
    imgCnv <*- b.If imgImage.Source Def
    setAttr false
  let setOpacity v = imgCnv.Opacity <- v
  showCheck false
  setOpacity sli.v.v
  checkCanvas => showCheck
  sli => fun v -> imgCnv |> opa v
  spBar |> tip' { 
    yield sf "不透明度スライダー: 重ね合わせる画像の不透明度( 現在 %.0f％ )を指定できます。" (sli.v *. 100.) }
  btn |> tip' { 
    yield sf "チェックボタン( %s ): ( 現在 重ね合わせている画像%s ) %s" 
      Shortcut.check.sKey (btn.v.If "あり" "なし") 
      (btn.v.If "重ね合わせている画像を消去します。" "画像をキャンバスに重ね合わせます。") }
  btn


let undoButton (pa:Panel) =
  let cnv  = Canvas() $ pa
  let  btn = IconButton(Bitmap.undo, shortcut = Shortcut.undo, coKey="8af", text="アンドゥ") $ tip' { yield sf "アンドゥボタン(%s): 描いたのを元に戻します" Shortcut.undo.sKey }
  let  sp  = StackPanel() $ composeMenuTopIf (fun _ -> swDraw.isMSec 100) cnv btn $ "fff".bg
  for i in 4 .. -1 .. 0 do 
    let lbl = Button' (sf "バックアップ(%d操作ぐらい前)を開く" ((i+1)*10)) $ sp
    lbl =+ {
      let i = (Undo.idxCommand /. 10 - i) %^ 5
      Undo.loadBackup i }
  btn


let ini (spTop:StackPanel) (spBottom:StackPanel) () =
  Palette.UI.ini spPalette
  Undo.ini()
  Tools.ini()

  let btnSelect = tool Shortcut.select Bitmap.select  "範囲選択" $ spTop $ tip' { yield sf "範囲選択ボタン(%sキー): 範囲選択ツール" Shortcut.select.sKey } 
  let btnSpoit  = btn  Shortcut.spoit  Bitmap.syringe "スポイト" $ spTop $ tip' { yield sf "スポイトボタン(%sキー): ポインタの所にある色を選択します" Shortcut.spoit.sKey }
  let btnEraser = tool Shortcut.eraser Bitmap.eraser  "消しゴム" $ spTop $ tip' { yield sf "消しゴムボタン(%sキー): 消しゴムを選択します" Shortcut.eraser.sKey }
  let btnPen    = tool Shortcut.pen    Bitmap.pen     "ペン"     $ spTop $ tip' { yield sf "ペンボタン(%sー): ペンを選択します" Shortcut.pen.sKey }

  let btnUndo   = undoButton spBottom 
  let btnClear  = btn  Shortcut.clearCanvas Bitmap.clear   "クリア"    $ spBottom $ tip' { yield sf "クリアボタン(%sキー) キャンバスに描かれているものを全消去します" Shortcut.clearCanvas.sKey }
  let btnCheck  = checkButton spBottom
  let btnSave   = btn  Shortcut.saveCanvas  Bitmap.save    "保存"      $ spBottom $ tip' { yield sf "保存ボタン(%sキー): 現在の模写用の画像とキャンバスを画像ファイルとして保存します" Shortcut.saveCanvas.sKey }
  let btnAnswer = btn  Shortcut.answer      Bitmap.correct "答え合わせ" $ spBottom $ tip' { yield sf "答え合わせボタン(%sキー): すぐに答え合わせをします" Shortcut.answer.sKey }
  btnPen <*- true

  let toolSelect isDelay btn mode = 
    for __ in tools do __ = btn |>* __
    icnv.EditingMode <- mode
    if mode <> EditingMode.Select then
      setAttr isDelay

  let selectPen    isDelay = toolSelect isDelay btnPen    EditingMode.Ink
  let selectEraser isDelay = toolSelect isDelay btnEraser EditingMode.EraseByPoint
  let selectSelect()       = toolSelect false   btnSelect EditingMode.Select

  let ev (sc:Shortcut) (btn:IconButton) = 
    sc >< btn -? { V (swDraw.isMSec 100) }

  Stopwatch.state =+ { 
    btnAnswer.IsHitTestVisible <- Stopwatch.isCorrecting.n
    btnAnswer |> opa (If Stopwatch.isCorrecting 0.25 1.)
  }

  ev Shortcut.select btnSelect =+ { selectSelect() }
  ev Shortcut.spoit  btnSpoit  =+ { spoit }
  ev Shortcut.eraser btnEraser -%% false >=< radEraser -%% true => selectEraser
  ( ev Shortcut.pen btnPen >< 
    Shortcut.brush1 >< Shortcut.brush2 >< Shortcut.brush3 >< Shortcut.brush4 ) -%% false >=< 
  ( opaPen >< coPen >< radPen >< wnd.Loaded ) -%% true => selectPen 

  grid1.PreviewMouseRightButtonDown =+ { btnPen.v.If selectEraser selectPen false }

  ev Shortcut.undo btnUndo =+ { if IsShift then Undo.redo else Undo.undo }

  ev Shortcut.clearCanvas btnClear  =+ { Undo.clear true }
  ev Shortcut.saveCanvas  btnSave   =+ { saveDialog }
  ev Shortcut.check btnCheck        =+ { checkCanvas <*- checkCanvas.v.n }
  ev Shortcut.answer      btnAnswer =+ {
    Stopwatch.state <*- StopwatchState.Correcting }

  imgPath -? { V imgReset } =+ {
    checkCanvas <*- false
    Undo.clear false
  }   