﻿[<AutoOpen>]
module Canvas.Core
open FStap open __ open System.Windows.Controls


type EditingMode = InkCanvasEditingMode

type Brush = { color:s sh; opacity:f sh; radius: f sh }

let checkCanvas = sh false

let radEraser = pref.shF "eraserRadious" 10.
let coPen     = HSVColor "brushColor"
let radPen    = pref.shF "brushRadious" 3.
let opaPen    = pref.shF "brushOpacity" 1.

let iBrush = pref.shI "indexBrush" 0

let brushes = 
  let cs = [|"000"; "f00"; "00f"; "00f"|]
  let os = [|1.; 1.; 1.; 0.25|]
  let rs = [|3.; 3.; 3.; 50.|]
  [| 
  for i in 0..3 -> 
    ( pref.shS ("color"   + i.s) cs.[i],
      pref.shF ("opacity" + i.s) os.[i],
      pref.shF ("radius"  + i.s) rs.[i] ) |]
 
Shortcut.brush1 -%% 0 >=<
Shortcut.brush2 -%% 1 >=<
Shortcut.brush3 -%% 2 >=<
Shortcut.brush4 -%% 3 =>* iBrush

