﻿[<AutoOpen>]
module Canvas.HSVColor

open FStap open __
open System.Windows.Media


type HSVColor (key) as __ =
  let _h = pref.shF (sf "%sH" key) 0.
  let _s = pref.shF (sf "%sS" key) 1.
  let _v = pref.shF (sf "%sV" key) 0.
  let getColor() = ofHsv _h.v _s.v _v.v
  let _e = Event<Color>()
  let fire() = getColor() |> _e.Trigger
  do _h >< _s >< _v =+ { fire() }
  member __.e   = _e.Publish
  member __.h   = _h
  member __.s   = _s 
  member __.v'  = _v 
  member __.hsv = _h.v, _s.v, _v.v
  member __.co  = getColor()
  member __.Co (co:Color) = 
    let h,s,v = co.hsv
    if h.ok then _h <*- h
    _s <*- s
    _v <*- v
    fire()

//  member __.Equals (x:HSVColor) =
//    __.h.v  = x.h.v && 
//    __.s.v  = x.s.v && 
//    __.v'.v = x.v'.v

//  interface Color sh with
//    member __.v 
//     with get () = __.co 
//     and  set v  = _fire v
//    member __.fired   = e.Publish :> _ obs
//    member __.fire  v = _fire  v
//    member __.force v = _force v
//    member __.quiet v = _quiet v

