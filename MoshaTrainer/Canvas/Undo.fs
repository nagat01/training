﻿module Canvas.Undo
open FStap open __ open System.Windows
open System.Windows.Ink
open System.Windows.Input
open System.Windows.Media
open System.IO
open System.Collections.Generic


let _sumP (__:StylusPoint seq) = __ |> Seq.sumBy(fun p -> p.PressureFactor)
let _sumX (__:StylusPoint seq) = __ |> Seq.sumBy(fun p -> p.X)
let _sumY (__:StylusPoint seq) = __ |> Seq.sumBy(fun p -> p.Y)

type StrokeComparer() =
  interface IEqualityComparer<Stroke> with
    member __.Equals(x, y) =
      let ps0 = x.StylusPoints
      let ps1 = y.StylusPoints
      let da0 = x.DrawingAttributes
      let da1 = y.DrawingAttributes
      ps0.len    = ps1.len    &&
      _sumX ps0  = _sumX ps1  &&
      _sumY ps0  = _sumY ps1  &&
      _sumP ps0  = _sumP ps1  &&
      da0.Height = da1.Height && 
      da0.Color  = da1.Color
    member __.GetHashCode x = x.GetHashCode()

type CommandCanvas = 
  | AddRem of Stroke HashSet * Stroke HashSet * i
  | Move   of Rect * Rect * StrokeCollection * i
  member __.idx =
    match __ with
    | AddRem (_, _, i) | Move (_, _, _, i) -> i


// consts
let dirBackup   = pwd +/ Folders.backup
let pathStrokes = pwd +/ (Software + ".inkCanvas")                    


// vars
let undos = Stack<CommandCanvas>()
let redos = Stack<CommandCanvas>()


let mutable onUndoRedo = false
let idxCommand = sh 0


// funcs
let project (r0:Rect) (r1:Rect) =
  let m = Matrix.Identity
  m.Translate(-r0.X, -r0.Y)
  m.Scale(r1.w / r0.w, r1.h / r0.h)
  m.Translate(r1.X, r1.Y)
  m

let incIdxCommand<'__> = 
  idxCommand <*- idxCommand +. 1
    
let idxBackground<'__> =  
  icnv.Strokes |> Seq.takeWhile(fun __ -> __.DrawingAttributes.Height > 2.5) |> Seq.length

let push __ = 
  redos.Clear()
  if undos.Count > 300 then
    let arr = undos.array
    undos.Clear()
    for i in 200 .. -1 .. 0 do
      undos.Push arr.[i]
  undos.Push __

let addStrokes (__:Stroke seq) =
  let idx = idxBackground
  for __ in __ do
    if __.DrawingAttributes.Height > 2.5 then
      icnv.Strokes.Insert(idx, __)
    else
      icnv.Strokes.Add __

let undo<'__> =
  if undos.Count > 0 then
    onUndoRedo <- true
    let _i = undos.Peek().idx
    while undos.Count > 0 && undos.Peek().idx = _i do
      let cmd = undos.Pop()
      match cmd with
      | AddRem (adds, rems, _) ->
        for __ in adds do
          icnv.Strokes.Remove __ |> ig
        addStrokes rems
      | Move (r0, r1, ss, _) -> 
        ss.Transform(project r1 r0, false)
      redos.Push cmd
    onUndoRedo <- false

let redo<'__> =
  if redos.Count > 0 then
    onUndoRedo <- true
    let _i = redos.Peek().idx
    while redos.Count > 0 && redos.Peek().idx = _i do
      let cmd = redos.Pop()
      match cmd with
      | AddRem (adds, rems, _) -> 
        for __ in rems do
          icnv.Strokes.Remove __ |> ig
        addStrokes adds
      | Move (r0, r1, ss, _) -> ss.Transform(project r0 r1, false)
      undos.Push cmd
    onUndoRedo <- false

let clear (check:b) =
  incIdxCommand
  if icnv.EditingMode = EditingMode.Select && icnv.GetSelectedStrokes().Count > 0 then
    icnv.GetSelectedStrokes() |> icnv.Strokes.Remove
  elif icnv.Strokes.Count > 0 then
    if check.n || 
      ( let res = MessageBox.Show("本当にキャンバスをクリアしますか？クリア後にUNDOで元に戻せます", "クリア", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel)
        res = MessageBoxResult.OK ) 
    then
      icnv.Strokes.Clear()
      checkCanvas <*- false
      Stopwatch.reset

let saveStrokes path =
  use stream = new FileStream(path, FileMode.Create)
  icnv.Strokes.Save stream

let loadStrokes path = try' {
  incIdxCommand
  use stream = new FileStream(path, FileMode.Open, FileAccess.Read)
  icnv.Strokes.Clear()
  for __ in StrokeCollection stream do
    icnv.Strokes.Add __ }

let loadBackup i =
  let file = dirBackup +/ (sf "backup%d.inkCanvas" i)
  loadStrokes file

let tryMerge (_adds:_ seq) (_rems:_ seq) idx =
  let push() =
    let adds = HashSet(_adds, StrokeComparer())
    let rems = HashSet(_rems, StrokeComparer())
    adds.ExceptWith _rems
    rems.ExceptWith _adds
    AddRem(adds, rems, idx) |> push
  if undos.Count = 0 || undos.Peek().idx <> idx then
    push()
  else
    match undos.Peek() with
    | Move _ ->
      push()
    | AddRem(adds, rems, _) ->
      adds.UnionWith  _adds
      rems.UnionWith  _rems
      let common = HashSet(adds, StrokeComparer())
      common.IntersectWith rems
      adds.ExceptWith common
      rems.ExceptWith common
          

// events
icnv.pmdown =+ { incIdxCommand }

let mutable strokeChanged = false
icnv.Strokes.StrokesChanged -? { V (onUndoRedo.n && strokeChanged.n) } => fun e ->
  strokeChanged <- true
  for __ in e.Added do
    icnv.Strokes.Remove __ |> ig
  addStrokes e.Added
  tryMerge e.Added e.Removed.rev idxCommand.v
  strokeChanged <- false

icnv.SelectionMoving >=< icnv.SelectionResizing => fun e ->
  incIdxCommand
  Move (e.OldRectangle, e.NewRectangle, icnv.GetSelectedStrokes(), idxCommand.v) |> push

wnd.Loaded =+! {
  do! pathStrokes.fileOk 
  do! 1000
  loadStrokes pathStrokes }

wnd.Closing =+ { saveStrokes pathStrokes }

idxCommand => fun idx ->
  if idx % 10 = 0 && idx <> 0 then
    let i = idx / 10 % 5
    dirBackup.mkDir
    let file = dirBackup +/ (sf "backup%d.inkCanvas" i)
    saveStrokes file


let ini() = forceInit undos
