﻿[<AutoOpen>]
module Canvas.Tools
open FStap open __
open System.Windows.Ink


// vars
let swSave = Stopwatch'()
let swDraw = Stopwatch'()


// funcs
let mutable timeLast = 0
let mkCursorDelay isDelay rad co = immediate' {
  let time = &timeLast +=! 1
  if isDelay then
    do! 200
  do! ( timeLast = time )
  invoke wnd { icnv.Cursor <- mkCursor (rad * 2.4 * trMouse.sc') co } }
  
let setAttr isDelay =
  if icnv.EditingMode = EditingMode.Select then
    icnv.UseCustomCursor <- false
  else
    icnv.UseCustomCursor <- true
    let pen    = icnv.EditingMode = EditingMode.Ink
    let check  = checkCanvas.v
    let coPen  = If check "f00".co (coPen.co.setA opaPen.v)
    let radPen = If check 2. radPen.v
    let co     = If pen coPen "fff".co
    let rad    = If pen radPen radEraser.v / 4.
    mkCursorDelay isDelay rad co
    if pen then
      let attrs = icnv.DefaultDrawingAttributes
      attrs.Color <- co
      attrs.Width <- rad
      attrs.Height <- rad
    else
      let r = radEraser /. 4.
      icnv.EraserShape <- EllipseStylusShape (r, r)
  
let spoit<'__> =
  getColorOnScreen MousePos |> coPen.Co

let save f =
  let uies = uies [bdrCnv; lblCnv; sep0] 
  uies |%| collapsed
  if swSave.isMSec 100 && isCanvas.v && icnv.Strokes.Count > 0 then
    f ()
    swSave.restart
  uies |%| vis

let saveDialog<'__> = 
  save <| fun _ -> 
    saveImageFe pref grid1

let saveAuto<'__>   = 
  save <| fun _ ->
    match imgPath.v with
    | "" -> ()
    | path ->
      let name = ( try path.nameNoExt with _ -> path ).remsRegex "/?<>\:*|".ss 
      let name = name + "_" + now.S "yyyy年MM月dd日hh時mm分ss秒" + ".png"
      let archive = pwd +/ Folders.archive
      archive.mkDir
      let dir = archive +/ now.S "yyyy年MM月dd日"
      dir.mkDir
      saveFePng (dir +/ name) grid1


let ini() = 
  icnv.UseCustomCursor <- true
  ugridUpdated =+ { setAttr true }

  icnv.pmdown =+ { 
  if Stopwatch.isCorrecting.n then
    checkCanvas <*- false 
  if Stopwatch.state =. StopwatchState.Paused then
    Stopwatch.state <*- StopwatchState.Running }

  icnv.lDrag =+ { swDraw.restart }

  icnv.pmdown -%% true  >=< 
  icnv.pmup   -%% false >=< 
  icnv.mleave -%% false => fun b ->
    isMouseDown <- b

