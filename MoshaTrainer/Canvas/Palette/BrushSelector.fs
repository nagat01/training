﻿module Palette.BrushSelector

open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes
open Canvas


type BrushPanel(sc:Shortcut.Shortcut, i) as __ =
  inherit Grid()
  do 
    __ $ wh 50 23 $ mgn1 2 $ "fff".bg 
    |> tip' { V(sf "ブラシパネル: ここにブラシがストックされます %sキー でこのパネルを選択できます。" sc.sKey) }
  let circle = Ellipse() $ __ $ mgnl 2 $ stroke 0.5 "4000" $ aCenterLeft
  let border = Border()  $ __ $ bdr 2 "0fff" 
  let lblKey = BorderedText("", "0f0", 10.) $ __ $ aTopRight
  let _co, _opa, _rad = brushes.[i]
  let isSelected _ = iBrush.v = i
  let drawKey() = lblKey <*- keyToS sc.key
  let drawIsSelected() =
    let b = isSelected()
    lblKey.co <- (If b "f00" "7f7")
    border |> bdco (If b "0f0" "fff") 
  let drawColor()  = circle |> fill (_co.v.co.setA _opa.v)
  let drawRadius() = circle |> size1 _rad.v 
  do
    drawKey()
    drawIsSelected()
    drawColor()
    drawRadius()
    sc.v =+ { drawKey()}
    iBrush =+ { drawIsSelected() }
    iBrush -?? isSelected =+? {
      coPen.Co _co.v.co
      opaPen <*-* _opa
      radPen <*-* _rad }
    __.lmdown =+ { iBrush <*- i }
    coPen  -?? isSelected =+ { _co <*- coPen.co.str }
    opaPen -?? isSelected =>* _opa
    radPen -?? isSelected =>* _rad
    _co >< _opa =+ { drawColor() }
    _rad        =+ { drawRadius() }


let ini(pa:UniformGrid') =
  let ugBrushes = UniformGrid'(r=4) $ mgn1 2 $ pa
  for i, brush in [Shortcut.brush1; Shortcut.brush2; Shortcut.brush3; Shortcut.brush4].seqi do
    BrushPanel(brush, i) |>* ugBrushes

 