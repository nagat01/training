﻿[<AutoOpen>]
module Palette.BrushSize

open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes
open Canvas


let borderedText co = BorderedText("", co, 7., FontWeights.Normal) 


type RadiusPanel(_w:i, _h:i, ?ratio:f) as __ =
  inherit Grid()
  do __ $ wh _w _h $ mgn1 4 |> "fff".bg
  member val circle = Ellipse() $ __ $ aCenterLeft
  member val line   = Line()    $ __ $ stroke 0.5 "f00"
  member val value  = borderedText "000" $ __ $ aTopRight $ nohit
  member __.Rad r = 
    __.value  |> v (sf "%.1f" (r * (ratio |? 1.)))
    __.circle |> size1 r
    __.line   |> xy2 r 0 r _h
  member __.e = __.eMouseDownAndDrag |%> fun po -> within 0.5 _w.f po.X


module RadPenThin =
  let sli = RadiusPanel(108, 20, 0.05) $ tip "ペンの半径パネル(20倍): 20倍に拡大してペンの半径をクリック or ドラッグで変更できます。"
  let draw<'__> =
    radPen *. 20. |> sli.Rad
    sli.circle $ stroke 0.5 "4000" |> fill (coPen.co.setA opaPen.v) 
  draw
  coPen >< radPen >< opaPen =+ { draw }
  sli =>* fun v -> v / 20. |>* radPen 


module RadPen =
  let grid   = Grid()
  let  sli   = RadiusPanel(50, 50) $ grid $ tip "ペンの半径パネル: ペンの半径をクリック or ドラッグで変更できます。"
  let  sp    = StackPanel() $ grid $ mgn2 4 2 $ aBottomRight $ nohit
  let   lbls = [| for co in ["000"; "f00"; "0f0"; "00f"] -> borderedText co $ sp $ haRight |]
  let drawCircle<'__> =
    sli.Rad radPen.v
    sli.circle $ stroke 0.5 "4000" |> fill (coPen.co.setA opaPen.v) 
  let drawOpacity<'__> =
    lbls.[0] <*- sf "%.1f" opaPen.v
    lbls.[0].co <- sf "%x000" (within 0 15 (opaPen *. 16.).i)
  let drawRGB<'__> =
    let co = coPen.co
    for i, v in [1, co.R; 2, co.G; 3, co.B] do
      lbls.[i] <*- sf "%3d" v
  drawCircle
  drawOpacity
  drawRGB
  coPen >< radPen >< opaPen =+ { drawCircle }
  opaPen =+ { drawOpacity }
  coPen =+ { drawRGB }
  sli =>* radPen


module RadEraser =
  let sli = RadiusPanel(50, 50) $ tip "消しゴムの半径パネル: 消しゴムの半径をクリック or ドラッグで変更できます。"
  let draw<'__> = sli.Rad radEraser.v 
  sli.circle |> stroke 1 "000"
  draw
  radEraser =+ { draw }
  sli =>* radEraser


let ini(pa:UniformGrid') =
  let ug = UniformGrid'(r=2) $ pa
  RadPen.grid |>* ug
  RadEraser.sli |>* ug

