﻿[<AutoOpen>]
module Palette.BrushColor
open FStap open __ open System.Windows.Controls
open System.Windows.Shapes
open Canvas

type BitmapSlider(w, h, is2) as __ =
  inherit Canvas()
  let whRect = If is2 (sz (w / 5) (h / 5)) (sz w (h / 10))
  let whTr   = If is2 (po -(w / 10) -(h / 10)) (po 0 -(h / 20))
  let wb = mkWriteableBitmap w h
  do __ $ wh w h $ mgn2 4 8 |> "fff".bg
  member val img  = Image()     $ __ $ WpfHelpers.h h $ src wb
  member val rect = Rectangle() $ __ $ size whRect $ tr whTr
  member __.x x = __.rect |> mvx (x * w.f)
  member __.y y = __.rect |> mvy (y * h.f)
  member __.e =
    __.eMouseDownAndDrag -% { 
      let p = poMouseOn __
      let f v d = v / !.d |> within 0. 1.
      yield po (f p.X w) (f p.Y h) 
      }
  member __.write f = 
    let pxs = Array.zeroCreate (w * h)
    for y in 0 .. h - 1 do
      for x in 0 .. w - 1 do
        pxs.[x + y * w] <- f x y
    wb.writeAll pxs
  member __.pixels pxs = wb.writeAll pxs


module SaturationValue =
  let w, h = 100, 100
  let sli = BitmapSlider(w, h, true) $ tip "明度・彩度パネル: 明度と彩度をクリック or ドラッグで変更できます。"
  let drawColor<'__> =
    sli.x <-* coPen.s
    sli.y <-* coPen.v'
    sli.rect $ fill coPen.co |> stroke 1 (If (coPen.v'.v < 0.5) "fff" "000")
  let drawHue<'__> =
    let pxs = Array.zeroCreate (w * h)
    let dh = within 0. 1. coPen.h.v * 6. 
    let ih = dh.i
    let f = dh.frac
    for y in 0 .. h - 1 do
      let off = y * w
      let v' = y.f / sli.h
      let v = v' * 255. |> int
      for x in 0 .. w - 1 do
        let s = x.f / sli.w
        let p = v' * (1. - s) * 255. |> int
        let q = v' * (1. - f * s) * 255. |> int
        let t = v' * (1. - (1. - f) * s) * 255. |> int
        let inline rgbToI r g b = pxs.[x + off] <- 0xff000000 + (r <<< 16) + (g <<< 8) + b
        match ih with
        | 1 -> rgbToI q v p
        | 2 -> rgbToI p v t
        | 3 -> rgbToI p q v
        | 4 -> rgbToI t p v
        | 5 -> rgbToI v p q
        | _ -> rgbToI v t p
    sli.pixels pxs
  drawColor
  drawHue
  coPen =+ { drawColor }
  coPen.h =+ { drawHue }
  sli => fun p -> 
    coPen.s <*- p.X
    coPen.v' <*- p.Y


module Hue = 
  let w, h = 50, 180
  let sli = BitmapSlider(w, h, false) $ tip "色合いパネル: 色合いをクリック or ドラッグで変更できます。"
  let drawRect<'__> =
    let mkCo add = ofHsv ((coPen.h +. add) % 1.) 1. 1.
    sli.y <-* coPen.h
    sli.rect $ fill (mkCo 0.) |> stroke 1 (mkCo 0.5) 
  let pxs = Array.zeroCreate (w * h)
  for y in 0 .. h - 1 do
    let co = ofHsv (y.f / sli.h) 1. 1. |> toI
    let off = y * w
    for i in off .. off + w - 1 do pxs.[i] <- co
  sli.pixels pxs
  drawRect
  coPen =+ { drawRect }
  sli => fun po -> poY po |>* coPen.h


module Opacity =
  let w, h = 50, 180
  let sli = BitmapSlider(w, h, false) $ tip "不透明度パネル: 不透明度をクリック or ドラッグで変更できます。"
  let drawColor<'__> =
    let d = (sli.w / 4.).ceili
    let bg = [| "ccc".co; "fff".co |]
    let pxs = Array.zeroCreate(w * h)
    for y in 0 .. h-1 do
      let off = y * w 
      let opa = y.f / sli.h
      let c i = blend bg.[i ^^^ ((y / d) &&& 1)] (density opa coPen.co) |> toI
      let c0 = c 0
      let c1 = c 1
      for i in off      .. off + 12 do pxs.[i] <- c0
      for i in off + 13 .. off + 24 do pxs.[i] <- c1
      for i in off + 25 .. off + 37 do pxs.[i] <- c0
      for i in off + 37 .. off + 49 do pxs.[i] <- c1
    sli.pixels pxs
    sli.rect |> stroke 1 (If (coPen.v'.v < 0.5) "fff" "000")
  let drawOpacity<'__> = sli.y opaPen.v
  drawColor
  drawOpacity
  opaPen =+ { drawOpacity }
  coPen =+ { drawColor }
  sli => fun p -> opaPen <*- p.Y


let ini(pa:StackPanel) =
  let sp = HStackPanel() $ pa $ vaCenter 
  Hue.sli |>* sp
  Opacity.sli |>* sp
  SaturationValue.sli |>* pa

