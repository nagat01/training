﻿module Palette.UI

open FStap open __ open System.Windows.Controls
open Canvas


let ini (pa:StackPanel) = 
  RadPenThin.sli |>* pa
  let ug = UniformGrid'(c=2) $ pa
  Palette.BrushSelector.ini ug
  Palette.BrushSize.ini ug
  BrushColor.ini pa

