﻿module Speech
open FStap open __ open System


type Speech (name:s) =
  let timing = name.nameNoExt.f |? f.MaxValue
  let mutable did = false
  member __.speech = 
    if isWithin (timing - 0.5) timing Stopwatch.remain && Stopwatch.elapsed > 0.7 && did.n then
      did <- true
      me <*- Uri name
      me.Play()
  member __.ini = did <- false

type Speeches (dir:s) as __ =
  let speech = 
    try 
      let dir = pwd +/ dir
      if dir.dirOk then
        (pwd +/ dir).files "*.mp3" 
        |%> fun file -> Speech file
      else [||]
    with _ -> [||]
  member __.speak = for __ in speech do __.speech
  member __.ini   = for __ in speech do __.ini

let speeches = Speeches Folders.speech

