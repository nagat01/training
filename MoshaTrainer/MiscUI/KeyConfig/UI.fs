﻿[<AutoOpen>]
module KeyConfig.UI
open FStap open __ open System.Windows open System.Windows.Controls
open System.Windows.Input

let xu, yu = 76, 35
let wCanvas = xu * 10 + 20
let hCanvas = yu * 4


let lines = [
  [Key.D1; Key.D2; Key.D3; Key.D4; Key.D5; Key.D6; Key.D7; Key.D8; Key.D9; Key.D0]
  [Key.Q; Key.W; Key.E; Key.R; Key.T; Key.Y; Key.U; Key.I; Key.O; Key.P]
  [Key.A; Key.S; Key.D; Key.F; Key.G; Key.H; Key.J; Key.K; Key.L]
  [Key.Z; Key.X; Key.C; Key.V; Key.B; Key.N; Key.M] ]


type KeyButton(key:Key) as __ =
  inherit Grid()
  let mutable prevSc = None'<Shortcut.Shortcut>
  let _ = __ $ wh xu yu
  let _ = Border() $ __ $ bdr4 3 1 3 4 "000" $ wh xu yu
  let _ = 
    BorderedText(keyToS key, "8af", 13., FontWeights.UltraBold) 
    $ __ $ aTopLeft $ trX 2
  let lblDesc = Label'() $ __ $ mgnb 4 $ aBottomCenter $ "fff".fg
  do
    __.lClick( 
      ( fun _ -> "555".bg __), 
      ( fun _ -> "567".bg __),
      ( fun _ -> "666".bg __) 
      ) =+ { DropDown.show __ wCanvas.f __.po.X __.w __.h key}
    __.mleave =+ { DropDown.hide() }
  member __.set (sc:Shortcut.Shortcut) = 
    prevSc |%| fun __ -> __.Key Key.None
    lblDesc <*- sc.desc
    prevSc <*- sc
  member __.unset = 
    lblDesc <*- () 
    prevSc.none


// ui 
let dp           = DockPanel() $ "cfff".bg
let  cnvKeyboard = Canvas() $ aTopCenter $ wh wCanvas hCanvas $ "666".bg
let  keyButtons  = Dictionary()
let btnDefault   = Button'("デフォルトに戻す", hover="555", push="567") $ bdr 1 "000" $ wh 90 yu $ "fff".fg  

let po x y = po (y * 20 + x * xu) (y * yu)


let ini(pa:Panel)() =
  DropDown.ini()
  dp $ collapsed |>* pa
  cnvKeyboard |>* dp.top 
  for y, line in lines.seqi do 
    for x, k in line.seqi do
      keyButtons.[k] <- KeyButton(k) $ cnvKeyboard.addTo (po x y) $ zIdx (3 - y)
    if y = 3 then btnDefault |>* cnvKeyboard.addTo (po 7 3)

  for sc in Shortcut.scsAll do
    sc.keyChanged => fun (k0, k1) -> 
      keyButtons.find k0 |%| fun __ -> __.unset
      keyButtons.find k1 |%| fun __ -> __.set sc

  for sc in Shortcut.scsAll do
    keyButtons.find sc.key |%| fun __ -> __.set sc

btnDefault =+ { for sc in Shortcut.scsAll do sc.keyDefault }

