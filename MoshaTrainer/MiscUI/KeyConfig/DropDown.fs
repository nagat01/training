﻿module KeyConfig.DropDown
open FStap open __ open System.Windows.Controls
open System.Windows.Input
open System.Windows.Media


let xu, yu = 100, 20
let wCanvas = xu * 3 + 8
let hCanvas = yu * 8 + 8

let shortcutMenu = None'<Key>


type ShortcutButton(_shortcut:Shortcut.Shortcut) as __ =
  inherit Button'(hover="555", push="567")
  do __ |> wh xu yu
  let desc = _shortcut.desc.rep "\r\n" ""
  let sp = HStackPanel() $ __
  let  lbl0 = Label' (desc + " (") $ sp
  let  lblKey = BorderedText("", "8af", 13.) $ sp $ trY 1 $ mgnw 2
  let  lbl1 = Label' ")" $ sp
  let set key =
    let co = If (key = Key.None) "ccc" "fff"
    co.fg lbl0
    co.fg lbl1
    lblKey <*- keyToS key
  do
    set _shortcut.v.v
    _shortcut.v => set
    __ =+ { shortcutMenu |%|* _shortcut.v }
  member __.shortcut = _shortcut
 

let cnv  = Canvas() $ zIdx 1
let  bdr = Border() $ bdr 2 "000" $ corner 2 $ wh wCanvas hCanvas $ "666".bg

let ini() =
  bdr |>* cnv
  for x, scs in [Shortcut.scsImage; Shortcut.scsCanvas; Shortcut.scsTool].seqi do
    for y, sc in scs.seqi do
      ShortcutButton sc |> cnv.addTo (po (4 + xu * x) (4 + yu * y))


let hide() = 
  tryCast<Grid> cnv.Parent |%| fun __ -> 
    __.rem cnv 

 
let show (grid:Grid) wPanel (x:f) (w:f) (h:f) key = 
  hide()
  let w0 = w / 2.
  let w1 = wCanvas /. 2.
  let x =
    if x + w0 - w1 < 0. then -x
    elif x + w0 + w1 < wPanel then w0 - w1 
    else wPanel - x - w1 * 2.
  cnv.RenderTransform <- TranslateTransform(x, h)
  cnv |>* grid 
  shortcutMenu <*- key

