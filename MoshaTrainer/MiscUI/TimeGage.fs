﻿module TimeGage
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Media.Effects
open System.Windows.Shapes


// func
let (|S20|S30|S90|S180|S450|S900|S2400|) i =
  if   i <  20  then S20
  elif i <= 30  then S30
  elif i <  90  then S90
  elif i <  180 then S180
  elif i <  450 then S450
  elif i <  900 then S900
  else S2400

let timeUnit i = 
  match i with 
  | S20   -> 1 , 5
  | S30   -> 2 , 5
  | S90   -> 5 , 3
  | S180  -> 10, 3
  | S450  -> 15, 4
  | S900  -> 30, 5 
  | S2400 -> 60, 5

let convertColorChar c = 
  match c with | 'f'->'f' | 'e'->'c' | 'd'->'9' | 'c'->'6' | 'b'->'3' | _->'0'

let coGage remain =
  [ "cdf"; "cee"; "cfd"; "dfc"; "efb"; "ffa"; "fea"; "fda"; "fca"; "faa"; ]
    .rev
    .[within 0 9 (remain * 10.).i]

let coGageDark remain = coGage remain |> String.map convertColorChar


let bdrGage   = Border() $ bdr 0.25 "000" 
let  gridGage = Grid() 
let   cd0     = ColumnDefinition() 
let   cd1     = ColumnDefinition() 
let   bdrLeft = Border() $ bdr 0.5 "000"
let ugGage    = UniformGrid'()  


let ini(grid:Grid)() = 
  grid |> tip "残り時間ゲージ: ここに残り時間のゲージを表示します。"
  bdrGage |>* grid
  gridGage |>* bdrGage
  cd0 |>* gridGage.cd.Add
  cd1 |>* gridGage.cd.Add
  bdrLeft |>* gridGage
  ugGage |>* grid
  
  let drawLines() =
    let i   = Stopwatch.limit.v.i
    let smallUnit, bigUnit = timeUnit i
    let len = i / smallUnit |> max 1
    ugGage.clear
    ugGage.Columns <- len
    for i in 1 .. len do
      let isMajor t f = If (i % bigUnit = 0) t f
      Line(Effect = BlurEffect(Radius=0.25)) $ ugGage
      $ xy2 0 0 0 24 $ haRight
      $ stroke (isMajor 0.5 0.25) "000" 
      |> isMajor ig (dotted [0.25; 0.25])

  let drawGage() =
    let remain = 
      if Stopwatch.limit =. 0. 
      then 0.
      else Stopwatch.remain /. Stopwatch.limit |> max 0.
    cd0 |> wStar remain 
    cd1 |> wStar (1. - remain)
    bdrLeft  $ (coGage remain).bg |> bdr 0.5 (coGageDark remain) 
 
  drawLines()
  drawGage()
  Stopwatch.limit =+ { drawLines() }
  Stopwatch.timer >< imgPath -? { V imgReset } =+ { drawGage() }

