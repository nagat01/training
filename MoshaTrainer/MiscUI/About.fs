﻿module __.About

open FStap open __ open System.Windows.Controls


let dp   = DockPanel()  $ "ccfb".bg
let sp   = StackPanel() $ mgn1 6 $ haCenter
let  hsp = HStackPanel() $ mgn1 6 $ haCenter
let   ico= Ico.MoshaTrainer 
let   t0 = BorderedText("模写トレーナー v0.9.8", "000", 18.) 
let  t1  = BorderedText("Copyright © nagat01. All rights reserved.", "000", 12.) $ haCenter
let  t2  = BorderedText("下記の使用許諾契約書をよくお読みの上、使用許諾契約書に同意された場合のみ使用してください。", "000", 10.) $ mgnt 6 $ haCenter
let  tbx = TextBox() $ wh 500 400 $ fsz 9 $ vscrAuto $ hscrAuto $ mgnt 12 $ haCenter

let ini(pa:Panel)() =
  dp $ collapsed |>* pa
  sp |>* dp
  hsp |>* sp
  ico |>* hsp
  t0 |>* hsp
  t1 |>* sp
  t2 |>* sp
  tbx |>* sp
  

tbx.IsReadOnly <- true
tbx.IsReadOnlyCaretVisible <- false
tbx <*- contract

