﻿module TimeLimit
open FStap open __ open System open System.Windows open System.Windows.Controls

 
let sp         = StackPanel() 
let  grid      = Grid() $ wh 130 44 $ "000".bg 
let   lblTimer = Label() $ fsz 32 $ bold $ "cff".fg
let  cnv       = Canvas()
let   btn      = ToggleButton("時間制限あり", on="99f", bgon="eef", def=true) 

let update<'__> =
  if Stopwatch.isTimeLimit.v then
    if Stopwatch.isCorrecting.n then
      lblTimer $ hcaRight $ padw 6 <*- Stopwatch.time
    else
      lblTimer $ hcaCenter $ padw 0 <*- "自己採点"
    match Stopwatch.state.v with 
    |  StopwatchState.Running -> lblTimer |> "fa0".fg 
    | _ -> lblTimer |> "cff".fg 
  else
    lblTimer $ hcaCenter $ padw 0 $ "cff".fg <*- "無制限"
  lblTimer.IsHitTestVisible <- Stopwatch.isCorrecting.n
  btn.v <- Stopwatch.isTimeLimit.v

let ini(add:Canvas -> _) = 
  cnv |> add |> ig
  sp |> composeMenu cnv grid
  lblTimer $ grid |>* tip' { V ( if Stopwatch.isTimeLimit.v then "制限時間タイマー: これを押すと、タイマーの停止と再開が出来ます。" else "無制限解除: これを押すと、時間無制限を解除します。" ) }
  btn $ sp |>* tip "時間制限ありボタン: 時間制限のあり/なしを指定できます。"
  update
 
  Stopwatch.timer =+ { lblTimer <*- Stopwatch.time }
  Stopwatch.state >< Stopwatch.limit >< Stopwatch.isTimeLimit =+ { update }
 
  lblTimer.mdown =+ {
  if Stopwatch.isTimeLimit.v then
    match Stopwatch.state.v with
    | StopwatchState.Running -> Stopwatch.state <*- StopwatchState.Paused
    | StopwatchState.Paused  -> Stopwatch.state <*- StopwatchState.Running
    | _ -> ()
  else
    if Stopwatch.state =. StopwatchState.Running then
      Stopwatch.state <*- StopwatchState.Paused
    Stopwatch.isTimeLimit <*- true  
  }

  btn => fun b ->
    if b && Stopwatch.state =. StopwatchState.Running then
      Stopwatch.state <*- StopwatchState.Paused
    Stopwatch.isTimeLimit <*- b 

 