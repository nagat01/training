﻿module Menu
open FStap open __ open System.Windows open System.Windows.Controls
open ImageList


let limits = [3;5;10;15;20;30;45;60;90;120;180;240;300;450;600;900;1200;1800;2700;3600] |%> float

let keyIconButton sc coKey img co = IconButton(img, co, shortcut = sc, coKey = coKey)

let iconButton src bg = IconButton (src, co=bg)


module AutoSave =
  let cnv       = Canvas() 
  let  btn      = IconButton(Bitmap.autoSave, key= "autoSave", coon = "fdb", opaoff = 0.25) 
  let  sp       = StackPanel() $ "fff".bg
  let   btnOpen = Button' "アーカイブを開く" 
  btnOpen =+ { launch (pwd +/ Folders.archive) }
  let ini(pa:Panel) =
    cnv |>* pa
    sp |> composeMenu cnv btn
    btn |>* tipIconButton "自動保存" "する" "しない" "" (sf "自動保存ボタン:%sオンにすると、模写が完了した画像を自動保存します。")
    btnOpen $ sp |>* tip "アーカイブを開くボタン: 自動保存した練習結果画像のあるフォルダを開きます。"


let  dpGuide   = DockPanel()
let  spPoint   = HStackPanel() 
let   lblScore = Label' "今日の得点:" $ "70f0".fg $ padl 4
let   lblPoint = Label'()            $ "70f0".fg $ bold $ pad4 4 0 12 0
let  spIndex   = HStackPanel() 
let   lblNumber= Label' "画像番号:" $ "7f00".fg 
let   lbl      = Label'()    $ "7f00".fg $ bold $ pad4 4 0 12 0 
let tbxGuide   = TextBlock() $ "aaf".fg $ bold $ tip "操作ガイド: ここに操作ガイドを表示します。"
let sp2        = DockPanel() $ "fff".bg $ vaTop $ haLeft
let  nbTime    = NumericButton (limits, Stopwatch.limit, trail= "秒", w= 40.) $ zIdx 1 $ "fff".bg 

let  volume    = VolumePanel() 

let  btnFolder    = iconButton Bitmap.openDirFiles "fec" 
let  btnFiles     = iconButton Bitmap.openFiles    "dfb" 
let  btnBackImage = keyIconButton Shortcut.prevImage "fa8" Bitmap.arrowImagePrev "fdb"
let  btnBack      = keyIconButton Shortcut.prevImage "8af" Bitmap.arrowPrev "bdf"     
let  btnNext      = keyIconButton Shortcut.nextImage "8af" Bitmap.arrowNext "bdf"     
let  btnNextImage = keyIconButton Shortcut.nextImage "fa8" Bitmap.arrowImageNext "fdb"

let  btnList     = iconButton Bitmap.imageList "dbf"  
let  btnLoadList = iconButton Bitmap.openListFile "fcf"
let  btnSaveList = iconButton Bitmap.saveListFile "fcf"
let  btnShuffle  = iconButton Bitmap.shuffleFile "ffe7d0"
let  btnClear    = iconButton Bitmap.clearImages "fcc"
let  btnMany     = IconButton(Bitmap.manyFiles, key="isMany", coon = "bbf", opaoff = 0.25) 

let  btnStatus = iconButton Bitmap.stats  "fff" 
let  btnPref   = iconButton Bitmap.wrench "fff" 
let  btnAbout  = iconButton Bitmap.about "fff"


let ini (dpMenu:DockPanel) () =
  ImageList.eImageNumber =+ { ImageList.sImageNumber |>* lbl }

  let drawSumPoint v = sf "%.1f点" v |>* lblPoint
  wnd.Loaded =+ { sumPoint |*> drawSumPoint }
  sumPoint => drawSumPoint 

  let drawTimeLimit b =
    nbTime.lblValue |> (If b "f77" "777").fg
    nbTime |> opa (If b 1. 0.25)
    nbTime.IsEnabled <- b
  Stopwatch.isTimeLimit |*> drawTimeLimit
  Stopwatch.isTimeLimit => drawTimeLimit

  volume.v |*> me.set_Volume
  volume => me.set_Volume 

  dpMenu $ vaTop $ haStretch $ zIdx 2 $ "fff".bg |> ig
  TimeLimit.ini(fun __ -> __ $ dpMenu $ vaTop)
  dpGuide |>* dpMenu.top 
  spPoint $ dpGuide |>* tip "今日の得点: 描けば描くほど増える、その日の得点を表示します"
  lblScore |>* spPoint 
  lblPoint |>* spPoint 
  spIndex $ dpGuide |>* tip "画像番号: 画像リストの何枚目を表示しているかを表示します"
  lblNumber |>* spIndex 
  lbl |>* spIndex 
  tbxGuide |>* dpGuide

  sp2 |>* dpMenu.top 
  nbTime $ sp2 |>* tip "制限時間アップダウン: １枚を描くための制限時間を指定できます。"

  volume $ sp2 |>* tip'' (fun __ -> sf "ボリュームボタン: 読み上げの音量 ( 現在 %.0f％ ) を調整できます。" (__.v *. 100.))

  btnFolder     $ sp2 |>* tip "フォルダを開くボタン: フォルダを選択して、フォルダ内の画像を画像リストに追加します。"
  btnFiles      $ sp2 |>* tip "ファイルを開くボタン: 複数の画像ファイルを選択して画像リストに追加します。"
  btnBackImage  $ sp2 |>* tip' { yield sf "画像だけ前のに変更ボタン: １つ前の画像をすぐに表示します。( Shift + %sキー でも出来ます。 )" Shortcut.prevImage.sKey }
  btnBack       $ sp2 |>* tip' { yield sf "前の画像ボタン: １つ前の画像をすぐに表示します。( %sキー でも出来ます。 )" Shortcut.prevImage.sKey }
  btnNext       $ sp2 |>* tip' { yield sf "次の画像ボタン: １つ次の画像をすぐに表示します。( %sキー でも出来ます。 )" Shortcut.nextImage.sKey }
  btnNextImage  $ sp2 |>* tip' { yield sf "画像だけ次のに変更ボタン: １つ次の画像をすぐに表示します。( Shift + %sキー でも出来ます。 )" Shortcut.nextImage.sKey }

  btnList     $ sp2 |>* tip "画像リストボタン: 読み込んでいる画像を表示します。"
  btnLoadList $ sp2 |>* tip "画像リスト開くボタン: 画像リストファイル(.imgs)を開きます。"
  btnSaveList $ sp2 |>* tip "画像リスト保存ボタン: 画像リストをファイル(.imgs)として保存します。"
  btnShuffle  $ sp2 |>* tip "シャッフルボタン: 画像リストの画像をシャッフルします。"
  btnClear    $ sp2 |>* tip "消去ボタン: 画像リストの画像を消去します。"

  btnMany     $ sp2 |>* tip'' (fun __ -> let a,b,c,d = __.v.If ("オン", 1000, "オフ", 200) ("オフ", 200, "オン", 1000) in sf "沢山ボタン: ( 現在 沢山読み込み%s ) で %d 枚読み込めます。%sにすると、最大 %d 枚読み込めるます。" a b c d)
  AutoSave.ini sp2

  btnStatus $ sp2 |>* tip "練習記録へ: 練習記録を表示する画面に移動します"
  btnPref   $ sp2 |>* tip "設定画面へ: 設定画面に移動します"
  btnAbout  $ sp2 |>* tip "About画面へ: 模写トレーナーのAbout画面に移動します"


let swapVis state =
  panelState <*-
    If ( panelState =. state ) 
      PanelState.Canvas
      state

let swapImageListVis<'__> =
  panelState <*- 
    if panelState =. PanelState.ImageList then PanelState.Canvas
    elif ImageList.Core.nImage > 0 then PanelState.ImageList
    else panelState.v


sGuide => fun s -> tbxGuide.Text <- s

btnFolder -%% true >=< 
btnFiles  -%% false => fun isDir -> 
  openFilesAllExt pref "folderImages" extsImage isDir (ImageList.addImages true)
btnBackImage >< Shortcut.prevImage.shift   =+ { ImageList.move false -1 }
btnNext      >< Shortcut.nextImage.noShift =+ { ImageList.move true  1 }
btnBack      >< Shortcut.prevImage.noShift =+ { ImageList.move true  -1 }
btnNextImage >< Shortcut.nextImage.shift   =+ { ImageList.move false 1 }

btnList     =+ { swapImageListVis }
btnLoadList =+ { openFile pref "folderLists" "imgs" (ImageList.loadImages true) }
btnSaveList =+ { saveFile pref "folderLists" "imgs" ImageList.saveImages }

btnShuffle =+ { ImageList.shuffleImages }

btnClear =+ {
  let res = MessageBox.Show("本当に画像リストを全て消去しますか？(この操作では画像ファイルは削除されません）", "画像リストのクリア", MessageBoxButton.OKCancel, MessageBoxImage.Question)
  if res = MessageBoxResult.OK then
    ImageList.clear
    panelState <*- PanelState.Canvas
  }

btnStatus =+ { swapVis PanelState.Status }
btnPref   =+ { swapVis PanelState.Pref }
btnAbout  =+ { swapVis PanelState.About }