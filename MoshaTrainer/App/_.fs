open FStap open __ open System open System.Windows open System.Windows.Controls
[<product "MoshaTrainer"; version "0.9.8"; company "ながと"; copyright "Copyright © ながと 2016";
  title "MoshaTrainer: パソコン内の画像で模写練習するためのツール"; STAThread>] SET_ENG_CULTURE

let start = now
let ts = System.Collections.Generic.Queue()

let init isAgreement =
  let dp = DockPanel() $ wnd $ lastChildFill $ "555".bg 
  me |>* dp
  let dpMenu = DockPanel() $ dp.top
  let gridRoot = Grid() $ dp.top

  let early = [
    "State", State.ini
    "Stopwatch", Stopwatch.ini
    "Shortcut", Shortcut.ini
    "MainPanel", MainPanel.ini gridRoot
    "Image", Image.UI.ini spImg
    "Canvas", Canvas.UI.ini spCnvTop spCnvBottom
    "Menu", Menu.ini dpMenu
    "TimeGage", TimeGage.ini gridGage
    ]

  let lately = [
    "About", About.ini gridRoot
    "KeyConfig", KeyConfig.UI.ini gridRoot
    "ImageList", ImageList.UI.ini gridRoot
    "Status", Status.UI.ini gridRoot CanvasArea.icnv
    "Events", Events.ini ]

  let mutable prev = now
  ts.enq ("startEarly", now - start)
  let init inis =
    for title, ini in inis do
      ini()
      ts.enq (title , (now - prev))
      prev <- now
  init early
  ts.enq ("finEarly", now - start)

  if isAgreement then
    wnd.Loaded =+ { 
    init lately
    ts.enq ("finShared", now - start)
    //for title, elapsed in ts do
    //  dfn "%i -- %s" elapsed.tmsec.i title
    }
  else
    init lately


trace true {

Constants.ini()

if pref.b "agreement" |? false then
  init true
else
  pref.isSave <- false
  wnd.WindowStartupLocation <- WindowStartupLocation.CenterScreen
  let wndContract = wndContract wnd pref
  wndContract.no =+ { 
    wnd.Close() }
  wndContract.yes =+ { 
    pref.isSave <- true
    pref ? ("agreement") <- true
    init false
    wnd $ ttl SoftwareName $ wh 800 600 |> ig
  }
wnd.run

}
