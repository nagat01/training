﻿module Events
open FStap open __ open System open System.Windows.Controls
module ImageList = ImageList.ImageList 


let ini() = 
  Stopwatch.timer =+! {
  if Stopwatch.isRunning then
    if Stopwatch.remain > -0.05 then
      Speech.speeches.speak
    else
      Media.SystemSounds.Beep.Play()
      Stopwatch.state <*- StopwatchState.Correcting }

  panelState =+ {
  let panels:Panel list = [About.dp; KeyConfig.UI.dp; ImageList.UI.grid; Status.UI.dp]
  let f i = for __ in panels do __.Vis(panels.[i] = __)
  match panelState.v with
  | PanelState.Canvas -> panels |%| collapsed
  | PanelState.About -> f 0
  | PanelState.Pref -> f 1
  | PanelState.ImageList -> f 2
  | PanelState.Status -> f 3 }

  Stopwatch.state -?? (=)StopwatchState.Correcting =+ { 
  if isSeparate.v || isCanvas.v then
    stamped.Trigger()
    Canvas.Core.checkCanvas <*- true
    if Menu.AutoSave.btn.v then 
      Canvas.Tools.saveAuto
  else
    ImageList.move true 1 }

  gridImg.pmwheel >=< gridCnv.pmwheel => fun e ->
    if Stopwatch.isCorrecting && ImageList.UI.grid.vis.n && ImageList.Core.nImage >= 2 then
      (e.Delta > 0).If -1 1 |> ImageList.move true 

  imgPath >< panelState =+ {
    wnd.Title <- 
      match panelState.v with
      | PanelState.Status -> sf "今日(%s)と最近１週間の練習記録" Status.Internal.sToday
      | _ -> imgPath.v }

  imgPath -? { V imgReset } =+ { Speech.speeches.ini }

  wnd.Closing =+ { 
  me.Stop()
  wnd.minimize } 

