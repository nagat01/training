﻿[<AutoOpen>]
module __.Guide
open FStap open __


let sGuide = sh ""

let  tip s (__:UIE) = __.hover =+ { sGuide <*- s }

let  tip'<'a when 'a :> UIE> = FuncBuilder(fun (f: u -> s) -> 
  fun (__:'a) -> __.hover =+ { sGuide <*- f() } )

let  tip'' f (__:#UIE) = __.hover =+ { sGuide <*- (f __:s) }

let tipButton lead a b trail f = tip'' (fun (__:ToggleButton) -> 
  sf " ( 現在 %s%s%s ) " lead (__.v.If a b) trail |> f)

let tipIconButton lead a b trail f = tip'' (fun (__:IconButton) -> 
  sf " ( 現在 %s%s%s ) " lead (__.v.If a b) trail |> f)

