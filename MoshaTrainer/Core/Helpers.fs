﻿[<AutoOpen>]
module __.Helpers
open FStap open System open System.Windows
open System.Globalization
open System.IO
open System.Net
open System.Windows.Input
open System.Windows.Media
   

let keyToS (__:Key) =
  let x = int __ 
  let f d c = int c + (x - d) |> char |> string
  if   isWithinLess 34 44 x then f 34 '0' 
  elif isWithinLess 44 70 x then f 44 'A' 
  else ""

let sToKey (__:s):Key =
  enum (
    if __ = "" then 
      0
    else
      let d = '0'.i
      let a = 'A'.i
      let x = __.[0].i
      if d <= x && x < d + 10 then 34 + x - d
      elif a <= x && x < a + 26 then 44 + x - a
      else 0 )


type ScaleTransform with
  member __.set sc (sz:Sz) =
    __.ScaleX  <- sc
    __.ScaleY  <- sc
    __.CenterX <- sz.w
    __.CenterY <- sz.h

let mkScaleTransform sc (sz:Sz) = ScaleTransform(sc, sc, sz.w, sz.h)

let formattedText s fsz fw (co:s) = 
  FormattedText(
    s, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, 
    Typeface(FontFamily(), FontStyles.Normal, fw, FontStretches.Normal), 
    fsz, co.br)


let saveImageTo src dest =
  if src = dest then mf "同じファイル %s に上書きすることは出来ません。" dest else
    try 
      if src.fileOk then
        File.Copy (src, dest)
      else
        use wc = new WebClient ()
        wc.DownloadFile (src, dest)
    with e ->
      mf "%s" e.Message


let saveImage (src:s) = 
  saveFile pref "saveImageItem" (src.ext.Replace(".","")) (saveImageTo src)
   

let mkCursor (r:f) (co:Color) =
  let n = max 5 (log (r * 1.5 + 1.) / log 2. + 1.).i
  let w = pown 2 n
  let half = w / 2
  use bmp = new Drawing.Bitmap(w, w) 
  let r = r / sqrt 2.
  for y, x in cart2 [0 .. w-1] [0 .. w-1] do
    let y' = y - half
    let x' = x - half
    let isInside r = (vec x' y').Length < r 
    let coDotted = (x+y).isEven.If 0xcc000000 0xccffffff
    let co =
      if (x' = 0 || y' = 0) && isInside (max 16. r) then
        coDotted
      elif isInside r then 
        co.i
      elif isInside (r + 1.) then
        coDotted
      else 0
    bmp.SetPixel(x, y, Drawing.Color.FromArgb co)
  bmpToCursor 0 bmp

