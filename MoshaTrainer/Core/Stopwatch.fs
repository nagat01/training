﻿module __.Stopwatch
open FStap


let timer = DispatcherTimer' 20
let limit = pref.shF "limit" 300.
let mutable elapsed = 0.
let mutable prev = now
let remain<'__> = limit.v - elapsed
let time<'__> = remain.sec.sShort true

let isTimeLimit = pref.shB "isTimeLimit" true
let state = sh StopwatchState.Paused
let isCorrecting<'__> = state =. StopwatchState.Correcting
let isRunning<'__> = timer.IsEnabled && state =. StopwatchState.Running

let reset<'__> = 
  elapsed <- 0.
  state <*- StopwatchState.Paused 


let ini() = 
  timer =+ {
  if 
    ( wnd.IsActive || ( ( wndSep |%> fun __ -> __.IsActive ) |? false) )
    && panelState =. PanelState.Canvas 
    && imgImage.v.ok && imgImage.bmpOk 
    && isRunning 
  then 
    &elapsed += (now - prev).tsec
  prev <- now }

  isTimeLimit >< state =+ {
    match isTimeLimit.v, state.v with
    | true, StopwatchState.Running -> 
      if timer.IsEnabled.n then 
        prev <- now
        timer.Start()
    | _ -> 
      timer.Stop()
    }

  imgPath -? { V imgReset } =+ { reset } 

