﻿[<AutoOpen>]
module __.State
open FStap open System.Windows.Controls
open System.Windows.Media.Imaging

[<RequireQualifiedAccess>] 
type PanelState = Canvas | ImageList | Status | Pref | About

[<RequireQualifiedAccess>] 
type StopwatchState = Running | Paused | Correcting

let mutable isMouseDown = false

let panelState = sh PanelState.Canvas
let isSeparate = pref.shB "isSeparate" false
let isCanvas   = pref.shB "isCanvas" true
let isLeft     = pref.shB "isLeft" true
let isMaximize = pref.shB "isMaximize" false
let isFliped = sh false

let imgImage = Image()
let imgPath = pref.shS "imgPath" ""
let mutable imgReset = true
let szImage<'__> = tryCast<BitmapSource> imgImage.Source |%> fun __ -> __.psz

let stamped = Event<u>()
let sumPoint = sh 0.

let ini() = forceInit panelState
