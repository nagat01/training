﻿module __.Shortcut
open FStap open __
open System.Windows.Input

type Pref with
  member __.shKey key def =
    let v: Key sh = sh (__.[key] |%> sToKey |? def)
    v => fun v ->  __.[key] <- keyToS v
    v

type Shortcut(key:Key, desc:s) =
  let keyPref = sf "key%s" desc
  let _key = pref.shKey keyPref key
  let mutable prevKey = _key.v
  let _keyChanged = Event<Key*Key>()
  let _e = Event<KeyEventArgs>()
  do
    wnd.kdown >=< dpSep.kdown => fun __ -> 
      if _key =. __.Key then _e.Trigger __ 
    _key => fun __ -> 
      pref.[keyPref] <- keyToS __  
      _keyChanged.Trigger (prevKey, __)
      prevKey <- __
  member __.desc = desc
  member __.e = _e.Publish
  member __.shift   = _e.Publish -? { yield IsShift }
  member __.noShift = _e.Publish -? { yield IsShift.n }
  member __.v = _key
  member __.key = _key.v
  member __.Key k = _key <*- k
  member __.sKey = keyToS _key.v
  member __.keyDefault = _key <*- key
  member __.keyChanged = _keyChanged.Publish
 

let scsImage  = ra()
let scsCanvas = ra()
let scsTool   = ra()
let scsAll = seq { yield! scsImage; yield! scsCanvas; yield! scsTool }

let mkShortcut (scs:_ ra) desc key = Shortcut(key, desc) $ scs
let mk0 = mkShortcut scsImage
let mk1 = mkShortcut scsCanvas
let mk2 = mkShortcut scsTool

let zoomIn      = mk0 "ズームイン"   Key.W 
let zoomOut     = mk0 "ズームアウト" Key.E 
let imageLeft   = mk0 "画像を右へ"   Key.H 
let imageBottom = mk0 "画像を下へ"   Key.J 
let imageTop    = mk0 "画像を上へ"   Key.K 
let imageRight  = mk0 "画像を左へ"   Key.L 
let rotateLeft  = mk0 "画像を右回転" Key.I 
let rotateRight = mk0 "画像を左回転" Key.O 

let prevImage   = mk1 "前の画像" Key.B
let nextImage   = mk1 "次の画像" Key.G
let select      = mk1 "範囲選択" Key.A
let clearCanvas = mk1 "キャンバス\r\nクリア" Key.X
let check       = mk1 "チェック" Key.C
let saveCanvas  = mk1 "キャンバス\r\n保存" Key.V
let answer      = mk1 "答え合わせ" Key.Q

let spoit  = mk2 "スポイト" Key.S
let eraser = mk2 "消しゴム" Key.D
let pen    = mk2 "ペン"     Key.F
let undo   = mk2 "アンドゥ" Key.Z
let brush1 = mk2 "ブラシ１" Key.D1 
let brush2 = mk2 "ブラシ２" Key.D2
let brush3 = mk2 "ブラシ３" Key.D3
let brush4 = mk2 "ブラシ４" Key.D4


let ini() = forceInit scsAll

