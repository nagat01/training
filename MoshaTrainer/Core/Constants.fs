﻿[<AutoOpen>]
module __.Constants
open FStap open System.Windows open System.Windows.Controls

let Ico = ImageProvider 24.
type Bitmap = BitmapSourceProvider


let dummy = ref 0
let forceInit obj = dummy := !dummy ^^^ obj.GetHashCode()

let SoftwareName = "模写トレーナー v0.9.8"
let Software = "MoshaTrainer"
let extsImage = "jpg jpeg bmp png tiff gif ico"
let patsImage = "*.jpg|*.jpeg|*.bmp|*.png|*.tiff|*.gif|*.ico"

module Folders =
  let history = Software + "_history"
  let archive = Software + "_archive"
  let backup  = Software + "_backup"
  let speech  = Software + "_speech"

let wnd = Window(Title = SoftwareName) $ allowDrop 
let pref = Pref(Software, wnd.Closed.wait)
let me = MediaElement'()

let dpSep = DockPanel() 

let ini () = 
  mw <- wnd
  restorePosSz pref wnd

