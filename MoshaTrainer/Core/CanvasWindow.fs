﻿[<AutoOpen>]
module __.CanvasWindow
open FStap open __ open System.Windows


let mutable wndSep = None
let WndSep<'__> =
  match wndSep with
  | None ->
    let wnd = Window(Title=SoftwareName + " キャンバス") 
    wnd.Closing =+ { 
    wndSep <- None
    isSeparate <*- false }
    wndSep <- Some wnd
    wnd
  | Some wndSep -> wndSep

isSeparate =+ {
  if isSeparate.v.n then
    wndSep |%| fun __ -> __.Close()
}

wnd.Closing =+ { wndSep |%| fun __ -> __.Close() }


type UIElement with
  member __.eMouseDownAndDrag =
    let mutable isDown = false
    __.lmdown =+ { isDown <- true }
    wnd.lmup >< wnd.mleave >< dpSep.lmup >< dpSep.mleave =+ { isDown <- false }
    ( __.lmdown |%> fun e -> e.pos) >=< 
      ( ( ( wnd.mmove >=< dpSep.mmove ) -? { V isDown } ) |%> fun e -> e.posOn __ )
    |%> fun p -> Point(within 0. __.rw p.X, within 0. __.rh p.Y)