﻿[<AutoOpen>]
module __.Controls    
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Media
open System.Windows.Media.Imaging
open System.Windows.Shapes


let _composeMenu center top cond (cnv:Canvas) (btn:FE) (sp:Panel) =
  let border = Border() $ hidden $ bdr 0.5 "000"
  cnv $ w btn.w $ h btn.h |> cs [btn; border]
  sp |>* border 
  cnv.menter -?? cond =+ { vis border }
  cnv.mleave -? { V lPressed.n } >< 
  wnd.mup -? { V cnv.IsMouseOver.n } ><
  wnd.mleave ><
  border.Loaded =+ { 
    let w = Seq.max [ for uie in sp.cs -> uie.rw ]
    border $ collapsed 
    |> mv (If center ((-w + btn.rw) / 2.) 0.) (If top -sp.rh btn.rh)
    }

let composeMenu     cnv btn sp = _composeMenu true  false (fun _ -> true) cnv btn sp
let composeMenuZero cnv btn sp = _composeMenu false false (fun _ -> true) cnv btn sp
let composeMenuTop  cnv btn sp = _composeMenu true  true  (fun _ -> true) cnv btn sp
let composeMenuTopIf cond cnv btn sp = _composeMenu true  true cond cnv btn sp


type DrawingVisualElement() as __=
  inherit FrameworkElement()
  member val vs = VisualCollection __
  override __.VisualChildrenCount with get() = __.vs.Count
  override __.GetVisualChild(i:i) =
    if i < 0 || __.vs.Count <= i
    then ArgumentOutOfRangeException() |> raise
    else __.vs.[i]


type BorderedText (s:s, co:s, sz:f, ?fw, ?bdrco) as __ =
  inherit DrawingVisualElement()
  let mutable _s = s
  let mutable _co = co
  let fw = fw |? FontWeights.Bold
  let bdrco = bdrco |? "fff"
  let dv = DrawingVisual() $ __.vs
  let draw isResize =
    let text0 = formattedText _s sz fw bdrco
    let text1 = formattedText _s sz fw _co
    if isResize then
      __ |> wh text1.Width text1.Height
    use render = dv.RenderOpen()
    for x,y in [-0.8,0.; 0.8,0.; 0.,-0.8; 0.,0.8] do
      render.DrawText(text0, po x y)
    render.DrawText(text1, po 0 0)
  do draw true
  member __.v with set s =  _s <- s; draw true
  member __.co with set co = _co <- co; draw false


type IconButton
  ( src:BitmapSource, ?co:s, ?w:f, ?padw:f, 
    ?shortcut:Shortcut.Shortcut, ?coKey, 
    ?srcon:BitmapSource, ?coon:s,
    ?opaoff:f, ?opaon:f,
    ?text:s,
    ?key:s, ?vDef,
    ?v:b sh
    ) as __ =
  inherit DrawingVisualElement() 
  let _v =
    match v with
    | Some v -> v
    | _ ->
      let v = vDef |? false
      match key with | Some key -> pref.shB key v | _ -> sh v
  let mutable _textDrawn = false
  let co = co |? "fff" 
  let w = w |? 24.
  let padw = padw |? 2.
  let w0 = padw * 2. + w
  let text2 = text |%> fun s -> formattedText s 12. FontWeights.Normal "000"
  let coBG() = if _v.v then coon |? co else co

  do __ |> wh ((match text2 with Some text2 -> text2.Width + padw * 2. | _ -> 0.) + w0) w0
  let dv = DrawingVisual()

  let draw (co:s) =
    let v = _v.v
    let src = if v then srcon |? src else src
    use ctx = dv.RenderOpen()
    if v 
    then opaon |%| ctx.PushOpacity
    else opaoff |%| ctx.PushOpacity
    ctx.DrawRectangle(co.br, null, Rect(0., 0., __.w, __.h))
    ctx.DrawImage(src, Rect(padw, padw, w, w))
    if v 
    then if opaon.ok then ctx.Pop()
    else if opaoff.ok then ctx.Pop()
    shortcut |%| fun shortcut ->
      let s = keyToS shortcut.v.v
      let text0 = formattedText s 14. FontWeights.Bold "fff"
      let text1 = formattedText s 14. FontWeights.Bold (coKey |? "000")
      for x,y in [-0.8,0.; 0.8,0.; 0.,-0.8; 0.,0.8] do
        ctx.DrawText(text0, po (x+1.) (y-1.))
      ctx.DrawText(text1, po 1. -1.)
    text2 |%| fun text2 ->
      ctx.DrawText(text2, po w0 8)

  let clicked =
    __.lClick(
      ( fun _ -> coBG().co.dark(24).str |> draw ),
      ( fun _ -> draw "fcc" ),
      ( fun _ -> coBG() |> draw ) )
  do
    shortcut |%| fun sc -> sc.v =+ { coBG() |> draw }
    clicked =+ { _v <*- not _v.v }
    _v =+ { coBG() |> draw }
    coBG() |> draw
    dv |>* __.vs

  member __.v with get () = _v.v and set v = _v <*- v
  member val e = clicked -% { V _v.v }


type GageSlider(key, ini, _w, bg:s, fg:s, coLever:s) as __ =
  inherit Grid()
  let _v = pref.shF key ini 
  do __ $ w (_w + 10.) |> bg.bg
  let cnv         = Canvas()    $ __  $ wh _w 20 $ vaCenter
  let  rectLine   = Rectangle() $ cnv $ wh _w 5 $ mvy 7.5 $ fill "fff"
  let  rectVolume = Rectangle() $ cnv $ h 5     $ mvy 7.5 $ fill fg 
  let  rectLever  = Rectangle(RadiusX=2., RadiusY=2.) $ cnv $ wh 8 20 $ fill "fff" $ stroke 1 coLever
  let move v =
    let x = v * _w
    rectLever  |> mvx (x - 4.)
    rectVolume |> w x
  do 
    _v |*> move
    _v => move
  member __.v = _v
  member __.e = ( __.lmdown >< __.lDrag ) -% { V (within 0. _w (poMouseOn rectLine).X / _w $ _v) } 


type VolumePanel() as __ =
  inherit HStackPanel()
  do __ $ "cfc".bg $ vaStretch |> tip "読み上げボリューム: 残り時間の読み上げのボリュームを変えられます。" 
  let _    = Label' Ico.speaker $ __ $ pad1 2
  let  sli = GageSlider ("volume", 0.15, 50., "cfc", "0c7", "0c7") $ __
  member __.v = sli.v
  member __.e = sli.e


type NumericButton< 'a when 'a: equality> (items: 'a list, _v:'a sh, ?lead, ?trail, ?w) as __ =
  inherit Canvas()
  let valueToIndex v = items |> Seq.tryFindIndex(fun item -> item = v)
  let mutable index = valueToIndex _v.v |? 0
  let select i = 
    index <- within 0 (items.Length - 1) i
    _v.fire items.[index]
  let add (x:_ opt) (sp:Panel) = x |%| fun x -> Label' x |>* sp
  let btnArrow f b = 
    let tri = Polygon() $ pnt3 (po 6 f) (po 0 b) (po 12 b) $ stroke 1 "77f" $ fill "ccf" $ rounded
    Button' tri $ pad2 2 1 $ vcaCenter $ vaStretch
  let spNUD    = HStackPanel() $ "fff".bg
  let  spLabel = HStackPanel() $ spNUD
  do    add lead  spLabel
  let   _lblValue = Label'() $ bold $ "f77".fg $ hcaRight $ spLabel $ fun __ -> w |%| __.W
  do    add trail spLabel
  let   ug = UniformGrid' (r=2) $ spNUD
  let    btnUp   = btnArrow 0 5 $ ug 
  let    btnDown = btnArrow 5 0 $ ug 
  do 
    Size(100., 50.) |> spNUD.Measure
    btnUp =+ { select (index - 1) }
    btnDown =+ { select (index + 1) }
  let bdrDropDown = Border() $ hidden $ bdr 0.5 "000" $ "fff".bg
  let  spDropDown = StackPanel() $ bdrDropDown
  let  bdrsItem = [ 
    for i, item in items.seqi -> 
      let bdr = Border() $ spDropDown $ bdr 2 "fff" $ haStretch
      let  sp = HStackPanel() $ bdr
      add lead sp
      let lbl = Label' item $ bold $ "f77".fg $ hcaRight $ sp  
      w |%| lbl.W
      add trail sp
      bdr.lmdown =+ { select i }
      bdr ]
  let setValue __ =
    bdrsItem |%| bdco "fff"
    items |> Seq.tryFindIndex(fun x -> x = __) |%| fun i -> bdrsItem.[i] |> bdco "7f7" 
    _lblValue.Content <- __
  do
    __ $ size spNUD.DesiredSize |> cs [spNUD; bdrDropDown]
    spLabel.lClick'() =+ { bdrDropDown.Vis bdrDropDown.vis.n }
    __.mleave -? { V lPressed.n } >< 
    wnd.mup -? { V __.IsMouseOver.n } >< wnd.mleave >< 
    __.Loaded =+ { 
      let w = Seq.max [ for uie in spDropDown.cs -> uie.rw ]
      bdrDropDown $ collapsed |> mv ((-w + spLabel.rw) / 2.) spLabel.rh }
    _v => setValue
    setValue _v.v


  member __.e = _v.fired
  member __.sp = spNUD
  member __.lblValue = _lblValue


type ToggleButton (ttl:o, ?bgon, ?bgoff, ?on, ?off, ?ttlon, ?opaon:f, ?opaoff, ?key, ?def) as __ =
  inherit Label'(ttl)
  let bgon   = bgon   |? "ccc"
  let bgoff  = bgoff  |? "fff"
  let on     = on     |? "444"
  let off    = off    |? "ccc"
  let ttlon  = ttlon  |? ttl
  let opaon  = opaon  |? 1.
  let opaoff = opaoff |? 1.
  let def    = def    |? false
  let mutable _v = false
  let f (co:s) _ = co.bg __
  let _e = __.lClick (f "cff", f "fdd", fun _ -> f __.bg ()) 
  do _e =+ { __.v <- _v.n }
  let set force v =
    if force || _v <> v then
      _v <- v
      __ $ v.If bgon.bg bgoff.bg |> v.If on.fg off.fg
      v.If ttlon ttl |>* __
      key |%| fun key -> pref ? (key) <- v
      __.Opacity <- v.If opaon opaoff
  do 
    __ $ bold $ pad2 4 2 $ on.fg $ bgoff.bg |> vcaCenter
    key |%| fun key -> pref.b key |? def |> set true
  member __.v 
    with get () = _v 
    and  set v  = set false v
  member __.bg = __.v.If bgon bgoff
  member __.e  = (_e >< wnd.Loaded) -% { V _v }

