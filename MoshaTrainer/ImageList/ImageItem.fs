﻿[<AutoOpen>]
module ImageList.ImageItem
open FStap open __ open System open System.Windows.Controls
open System.Windows.Media.Imaging


let mkThumbnail url =
  let __ = BitmapImage()
  __.BeginInit()
  __.DecodePixelHeight <- 240
  __.CacheOption   <- BitmapCacheOption.OnLoad
  __.CreateOptions <- BitmapCreateOptions.DelayCreation ||| BitmapCreateOptions.IgnoreColorProfile
  __.UriSource     <- Uri url
  __.EndInit()
  __


type ImageItem (path:s) as __ =
  inherit Grid()
  let _path = sh path
  let tblk = TextBlock(Text = _path.v)
  let index() = (__.Parent :?> WrapPanel).indexOf __
  let isSelected() = imgPath.v = _path.v
  let _setSize() =
    let sz = szII
    __ |> size1 sz
    tblk $ fsz (sz/20.) |> mah (sz/3.)
  let setBGLeave() = (isSelected().If "7f7" "0fff").bg __
  let setBGEnter() = (isSelected().If "afa" "7ecf").bg __
  do
    _setSize()
  member __.path with get () = _path.v and set v = _path <*- v
  member __.draw =
    if __.VisualChildrenCount = 0 then
      if isSelected() then "7f7".bg __ 
      let lbl       = Label'() $ __  $ "cfff".bg $ mgn1 8 $ aStretchStretch $ hcaCenter $ vcaBottom $ tip "クリックすると、その画像で３０秒ドローイング出来ます。"
      let  img      = Image(Source = mkThumbnail _path.v) $ lbl
      let bar       = DockPanel() $ __  $ mgn1 8. $ vaTop
      let  sp       = StackPanel() $ bar.right
      let   btnDel  = IconButton Bitmap.remove $ sp $ tip "削除ボタン: 画像リストから画像を削除出来ます。"
      let  _        = tblk $ bar.left $ vaTop $ wrap $ "9000".bg $ "fff".fg $ discreet 0.8 $ tip "クリックすると、画像を開くことが出来ます。"
      lbl.lclick =+ {
        panelState <*- PanelState.Canvas
        eChange <*- (index(), true, false) }
      btnDel =+? {
        eRemove <*- (index()) }
      __ |> light' 0.8 img 
      __.menter =+ { setBGEnter() }
      __.mleave =+ { setBGLeave() }
      tblk.lclick =+? { processPWD _path.v }
      _path =>* tblk

    member __.setSize = _setSize()
    member __.setBG = setBGLeave()
