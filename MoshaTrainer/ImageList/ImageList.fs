﻿[<AutoOpen>]
module ImageList.ImageList
open FStap open __
open System.Collections.Generic



let iis<'__> = Seq.cast<ImageItem> wp.cs 
let getII i = wp.cs.[i] :?> ImageItem

let eImageNumber = Event<u>()
let sImageNumber<'__> = if nImage <> 0 then sf "%d / %d" (iSelected + 1) nImage else ""

let move isReset delta = 
  eChange <*- (iSelected + delta, isReset, true)


let addImages isShow (paths:s seq) =
  let _ps = paths >>= fun p -> 
    if p.fileOk || p.urlOk then [|p|] 
    elif p.dirOk then p.files patsImage 
    else [||]
  let psPrev = [ for ii in iis -> ii.path ]
  let ps = _ps >%?~ psPrev.have
  if ps.len <> _ps.len then 
    msg "既に画像リストにあった一部、もしかしたら全部のファイルは追加されませんでした。"
  if ps.len > 0 then
    let limit = (pref.b "isMany" |? false).If 1000 200
    let q = Queue ps
    let mutable next = true
    while next && q.Count > 0 do
      if nImage < limit then 
        ImageItem q.deq |>* wp
      else
        mf "画像が%d枚になったので、これ以上追加できません。" limit
        next <- false
    if isShow && psPrev.len = 0 then
      eChange <*- (0, true, true)
    else
      eImageNumber <*- ()

let clear<'__> =
  wp.clear
  imgReset <- true
  imgPath <*- ""
  eImageNumber <*- ()

let shuffleImages<'__> =
  if nImage > 0 then
    let ii = getII(iSelected)
    let iis = iis.array.shuffle
    wp.clear
    iis |%|* wp
    eChange <*- (wp.indexOf ii, true, true)

let loadImages isShow (file:s) = try' { file.readLines |> addImages isShow }
let saveImages (file:s) = file.writeLines [ for ii in iis do if ii.path.pathOk then yield ii.path ]

