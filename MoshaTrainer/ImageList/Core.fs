﻿[<AutoOpen>]
module ImageList.Core
open FStap open __ open System.Windows.Controls

let mutable szII = 150.

let wp = WrapPanel()

let mutable iSelected = 0 
let nImage<'__> = if wp.cs.ok then wp.cs.len else 0

let eChange = Event<i*b*b>()
let eRemove = Event<i>()

