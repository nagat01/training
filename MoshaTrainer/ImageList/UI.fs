﻿module ImageList.UI
open FStap open __ open System.Windows.Controls


let grid         = Grid() $ "650a".bg 
let  scr = ScrollViewer() 
let  sp          = HStackPanel() $ aTopRight 
let   btnZoomIn  = IconButton Bitmap.zoomIn      
let   btnZoomOut = IconButton Bitmap.zoomOut     

let ini (pa:Panel) () =
  grid $ collapsed $ pa |>* tip "画像リスト: マウスホイールでスクロールできます。"
  scr |>* grid
  wp |>* scr
  sp |>* grid 
  btnZoomIn  $ sp |>* tip "ズームインボタン: 参考画像を拡大します。"
  btnZoomOut $ sp |>* tip "ズームアウトボタン: 参考画像を縮小します。"
  pwd +/ (Software + ".imgs") |> loadImages false
  let file = imgPath.v
  if file.pathOk then
    iis.tryFind (fun __ -> __.path = file) |%| fun ii ->
      iSelected <- iis.indexOf ii
      eImageNumber <*- ()

let scroll<'__> = 
  if panelState =. PanelState.ImageList then 
    immediate' {
    let mutable count = 20 
    while (&count -= 1; count >= 0) do
      do! 20
      try' {
      wp.cs.[iSelected].offset.Y - (grid.rh - szII) / 2.
      |> scr.ScrollToVerticalOffset 
      count <- 0 
      } }

let zoom isZoomIn =
  szII <- szII * (If isZoomIn 1.25 0.8) |> within 80. 240.
  for ii in iis do 
    ii.setSize

 
btnZoomIn -%% true >=< btnZoomOut -%% false => zoom

wnd.pmwheel => fun e ->
  if IsCtrl then
    e.Handled <- true
    e.Delta > 0 |> zoom

panelState =+ { scroll }

eChange => fun (i, isReset, isScroll) ->
  let i = i %^ nImage
  imgReset <- isReset
  imgPath <*- if i < nImage then getII(i).path else ""
  let setBG i = if i < nImage then getII(i).setBG
  setBG iSelected
  setBG i
  iSelected <- i
  eImageNumber <*- ()
  if isScroll then scroll

eRemove => fun i ->
  let iNext = iSelected + (i < iSelected).If -1 0
  wp.Children.RemoveAt i
  eChange <*- (iNext, i = iSelected, false)
  if panelState =. PanelState.ImageList && nImage = 0 then
    panelState <*- PanelState.Canvas

scr.ScrollChanged => fun e ->
  let n = nImage
  let hii = szII
  let h = e.ExtentHeight 
  let y = e.VerticalOffset
  let hv = e.ViewportHeight
  let f y = n *. y / h |> int
  for i in max 0 (f (y - y % hii)) .. min (f (y + hv + hii * 2.)) (n-1) do
    ( wp.cs.[i] :?> ImageItem ).draw

wnd.Closing =+ { pwd +/ (Software + ".imgs") |> saveImages } 

open System.Windows.Forms
wnd.Drop => fun e ->
  e.Data.GetData DataFormats.FileDrop |> tryCast |%| addImages true

