﻿module Image.Layout
open FStap open __ open System.Windows.Controls


let zoomImg<'__> = immediate' {
  let mutable c = 0
  while imgImage.szOk.n && &c +=! 1 < 100 do do! 10
  do! 10
  invoke wnd {
  let! sz = szImage
  let sc = ugrid.rh / sz.h * sz.w
  let scw = wGridMain / ugrid.Columns.f / sc
  let sch = 1. 
  let ma = max scw sch
  let mi = min scw sch
  isMaximize.v.If ma mi |> trMouse.Scale } 
  do! 10
  invoke wnd { trMouse.Move (vec 0 0) }
  }

let layout<'__> =
  CanvasArea.setUGrid
  zoomImg

let setCanvasState<'__> =
  let isSingle = isSeparate.v.n && isCanvas.v.n
  grid1.Vis isSingle.n
  spCnvTop.Vis isSingle.n
  spCnvBottom.Vis isSingle.n
  MainPanel.setMenu
  MainPanel.setBar
  if isSeparate.v then
    dpSep |>* WndSep
    WndSep.Show()
  
let setIsLeft<'__> =
  let isLeft = isLeft.v
  spImg       |> If isLeft haLeft haRight
  spCnvTop    |> If isLeft haRight haLeft
  spCnvBottom |> If isLeft haRight haLeft
  DockPanel.SetDock(spCnvBottom, If isLeft Dock.Right Dock.Left)


let btnCanvas (sp:StackPanel) =
  let btn = 
    IconButton(Bitmap.canvas, v = isCanvas, coon = "ffd", opaoff = 0.25) 
    $ tipIconButton "キャンバスを" "表示" "非表示" "中" (sf "キャンバスボタン:%s模写するためのキャンバスを表示するかしないかを指定します。")
  let cnv = Canvas() $ sp
  let sp = StackPanel() $ composeMenuZero cnv btn
  let btnIsSeparate = ToggleButton("キャンバスを分離", key="isSeparate", on="99f", bgon="eef", def=false) $ sp
  isSeparate <*- btnIsSeparate.v
  btnIsSeparate =>* isSeparate
  isSeparate =>* btnIsSeparate.set_v


let ini(sp:StackPanel) =
  btnCanvas sp 
  IconButton(Bitmap.flipHorizontal, v = isLeft, coon = "efe", opaoff = 0.25) 
  $ sp |>* tipIconButton "画像を" "左" "右" "側に表示" (sf "画像左右ボタン:%s画像を右側に表示するか、左側に表示するかを指定します。")
  IconButton(Bitmap.maximize, v = isMaximize, co = "bfb", opaoff = 0.25 ) 
  $ sp |>* tipIconButton "" "大き" "小さ" "めで表示" (sf "画像大きめボタン:%s新たに表示される参考画像を大きめに表示するか小さめに表示するかを決めます。")

  setIsLeft
  setCanvasState
  layout

  isSeparate >< isCanvas =+ { setCanvasState; layout }
  isLeft =+ { setIsLeft; layout }
  isMaximize =+ { zoomImg }

