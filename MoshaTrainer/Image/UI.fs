﻿module Image.UI
open FStap open __ open System.Windows.Controls
open System.Windows.Media.Imaging

let isMono = pref.shB "isMono" false
let mutable imgBitmap = None: BitmapSource opt

let btnMono = IconButton(Bitmap.rgbColor, v = isMono, co = "ccc", opaon = 0.25) 

let btnFlipImage = IconButton(Bitmap.flipImage) 
let btnZoomIn    = IconButton(Bitmap.zoomIn, shortcut = Shortcut.zoomIn, coKey = "8af")   
let btnZoomOut   = IconButton(Bitmap.zoomOut, shortcut = Shortcut.zoomOut, coKey = "8af") 
let btnInit      = IconButton(Bitmap.init) 


let updateBitmap() =
  let path = imgPath.v
  imgBitmap <- if path.ok then Some(bimgSafe path :> BitmapSource) else None
  if imgReset then trMouse.Init
  match imgBitmap with
  | Some bmp ->
    imgImage <*- if isMono.v then grayBmp bmp else bmp
    Layout.zoomImg
  | _ ->
    imgImage.Source <- null
  if imgReset then Rotate.rotateImg true


let ini(sp:StackPanel)() = 
  Layout.ini sp
  btnMono     $ sp |>* tipIconButton "" "白黒" "カラー" "で表示" (sf "白黒ボタン:%s参考画像をカラー表示にするか、白黒表示にするか決められます。")
  Deskel.Deskel.ini sp
  Outline.ini sp
  Rotate.ini sp
  Vanish.ini sp
  btnFlipImage $ sp |>* tip "画像左右反転ボタン: 参考画像とキャンバスの絵を左右反転します。"
  btnZoomIn    $ sp |>* tip' { yield sf "ズームインボタン(%sキー): 参考画像を拡大します。"   Shortcut.zoomIn.sKey }
  btnZoomOut   $ sp |>* tip' { yield sf "ズームアウトボタン(%sキー): 参考画像を縮小します。" Shortcut.zoomOut.sKey }
  btnInit      $ sp |>* tip "初期位置ボタン: 参考画像を初期位置に戻します。"
  
  updateBitmap ()
  Layout.layout


btnMono =>* isMono

btnFlipImage =+ { isFliped <*- isFliped.v.n }

(btnZoomIn  >< Shortcut.zoomIn) -%% true >=< 
(btnZoomOut >< Shortcut.zoomOut) -%% false => fun isZoomIn -> 
  isZoomIn.If 0.3 -0.3 |> trMouse.Zoom

btnInit =+ { 
  trMouse.Init 
  Layout.zoomImg
  Rotate.rotateImg true }

Shortcut.imageLeft   -%% vec -10 0 >=<
Shortcut.imageBottom -%% vec 0 10  >=<
Shortcut.imageTop    -%% vec 0 -10 >=<
Shortcut.imageRight  -%% vec 10 0 => fun __ -> 
  vec (isFliped.If -__.X __.X) __.Y |> trMouse.Move

Shortcut.rotateLeft  -%% -10. >=< 
Shortcut.rotateRight -%% 10. => fun __ -> 
  isFliped.If -__ __ |> trMouse.Rotate

isMono =+ {
  imgBitmap |%| fun bmp ->
    imgImage <*- if isMono.v then grayBmp bmp else bmp
}

imgPath =+ { updateBitmap() }

