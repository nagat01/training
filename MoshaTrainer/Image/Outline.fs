﻿module Image.Outline
open FStap open __ open System.Windows.Controls
open Algo

let idCanny = ref 0


let cnv     = Canvas()
let btn     = IconButton(Bitmap.outline, key = "isOutline", coon = "ccc", opaoff = 0.25 ) 
let  sp     = StackPanel() $ "fff".bg
let   spSli = HStackPanel() 
let   lbl   = Label' "輪郭の下の\r\n画像の不透明度" $ fsz 7 $ "fff".bg
let   sli   = GageSlider("outlineImageOpacity", 0.5, 50., "ddd", "777", "777") 

let ini (pa:Panel) = 
  cnv |>* pa
  sp |>* composeMenu cnv btn
  btn |>* tipIconButton "輪郭抽出" "オン" "オフ" "" (sf "輪郭抽出ボタン:%s参考画像を輪郭抽出する/しないを指定します。")
  spSli |>* sp
  lbl |>* spSli
  sli |>* spSli
  spSli |> tip' { yield sf 
    "不透明度スライダー: 輪郭の下の画像の不透明度 ( 現在 %.0f％ ) を指定します。" (sli.v *. 100.) }

let renew() = 
  idCanny := !idCanny + 1
  let id = !idCanny
  imgOutline.Source <- null
  imgOutline.Vis true
  tryImmediate' {
  do! 200
  do! id = !idCanny && btn.v && imgPath.v.ok
  let bmp = bimgSafe imgPath.v
  do! bmp.ok && id = !idCanny
  let w, h, ps, bps = bmp.pw, bmp.ph, bmp.pixelsI, bmp.Format.BitsPerPixel
  do! 1
  do! (id = !idCanny)
  let ps = if bps = 8 then Canny.bpp_8_32 w h ps else ps
  let! ps = Canny.filter idCanny id (18*18*384) (6*6*384) w h ps 
  do! 1
  do! (id = !idCanny)
  let wb = mkWriteableBitmap w h
  ps |> wb.writeAll
  imgOutline <*- wb
  imgImage |> opa sli.v.v 
  }

wnd.Loaded =+ { renew() }
imgPath =+! {
  do! 50
  renew() }
btn =+ { renew() }
btn >< sli =+ { imgImage |> opa (btn.v.If sli.v.v 1.) }

