﻿module Image.Rotate
open FStap open __ open System open System.Windows open System.Windows.Controls


let iRotateUnit = pref.shI "rotateUnit" 2

let settings = dict ["180度刻み",180.; "90度刻み",90.; "45度刻み",45.; "22.5度刻み",22.5; "完全ランダム",1.]


// ui
let cnv         = Canvas()
let  btn        = IconButton(Bitmap.rotate, key= "isRotate", coon = "ccc", opaoff= 0.25) 
let  sp         = StackPanel() 
let   lbx       = ListBox() 
let   btnNormal = ToggleButton("通常の角度あり", key = "isNormalRotate", on="99f", bgon="eef", def=true) 

let ini (pa:Panel) =
  cnv |>* pa
  sp  |>* composeMenu cnv btn
  btn |>* tipIconButton "回転" "する" "しない" "" (sf"回転ボタン:%s新たに表示される参考画像をランダムに傾けて表示するか、しないかを指定します。")
  lbx $ sp |>* itemsi iRotateUnit.v [ for s in settings.Keys -> Label' s $ tip (sf "角度を %s で回転させます。" s) ]
  btnNormal $ sp |>* tipButton "角度0度" "あり" "なし" "" (sf "角度０度の有無を指定:%s通常の角度0度でも配置するか/しないかを指定できます。")


// func
let rotateImg noDiff = invoke wnd {
  let! diff = (lbx.item :?> ContentControl).Content :?> s |> settings.find
  let normal = btnNormal.v && noDiff
  let angles = [| If normal 0. diff .. diff .. 359. |]
  btn.v.If angles.rand 0. |> trMouse.Angle }


// events
btn                            =+ { rotateImg false }
btnNormal.lmdown >< lbx.lmdown =+ { btn.v  <- true }
btnNormal >< lbx               =+ { rotateImg true }

lbx =+ { iRotateUnit <*- lbx.idx }

