﻿module Deskel.Selector
open FStap open __ open System.Windows open System.Windows.Controls
open System.Windows.Media


let Lines  = [1;2;3;4;5;6;8;10;12;14;16;18;20;24;28;32]
let Colors = [ 
  "77f 77f 77f"
  "7f7 7f7 7f7"
  "f77 f77 f77"
  "ff0 ff0 ff0"
  "f0f f0f f0f"
  "0ff 0ff 0ff"
  "000 000 000" 
  "3f6 69f f96"
  "0bb f66 c0f"
  "99d fc0 0cc"
  "d6d 6f6 df0"
  "e88 6c6 0bf"
  "fc0 06f 6f0"
  "777 444 000" 
  ]


let iColor  = pref.shI "deskelColor" 0
let hDeskel = pref.shI "hDeskel" 4
let vDeskel = pref.shI "vDeskel" 4

let getCoordinate (p:Po) = p.X.i / 28, p.Y.i / 68

let getColor i j = Colors.[i].Substring(j*4, 3)


let cnv         = Canvas()
let  btn        = IconButton(Bitmap.deskel, key = "isDeskel", coon = "cdf", opaoff = 0.25, vDef = true) 
let  sp         = StackPanel() $ "fff".bg
let   spLine    = HStackPanel() $ zIdx 1
let    nbHLine  = NumericButton<i>(Lines, hDeskel, "横", "分割", w=20.)
let    nbVLine  = NumericButton<i>(Lines, vDeskel, "縦", "分割", w=20.)
let   grid      = Grid() $ wh (28 * 7) (68 * 2) $ "fff".bg 
let    dveColors= DrawingVisualElement()  
let   btnNumberAlphabet = ToggleButton("数字・アルファベット", key = "showNumberAlphabet", on="99f", bgon="eef", def=true) 
let   btnIsRelative = ToggleButton("参考画像に合わせる", key = "isDeskelRelative", on="99f", bgon="eef", def=false)

let ini (pa:Panel) =
  cnv |>* pa
  sp |>* composeMenu cnv btn
  btn |>* tipIconButton "デスケル" "あり" "なし" "" (sf "デスケルボタン:%s比率を確認するための補助線を表示するかしないかを指定します。")
  spLine |>* sp
  nbHLine $ spLine |>* tip "デスケルの横分割の数: デスケルで横に分割する数を指定できます。"
  nbVLine $ spLine |>* tip "デスケルの縦分割の数: デスケルで縦に分割する数を指定できます。"
  grid $ sp |>* tip "デスケルの色: ３種類のデスケルの線の色を指定できます。"
  dveColors |>* grid
  btnNumberAlphabet $ sp |>* tipButton "数字・アルファベット" "あり" "なし" "" (sf "数字・アルファベット:%sデスケルの端に数字、アルファベットを表示するか/しないかを指定できます。")
  btnIsRelative $ sp |>* tipButton "参考画像に合わせる" "ON" "OFF" "" (sf "参考画像に合わせる:%sデスケルを参考画像に合わせて配置する/しないを指定できます。")

(
let dv = DrawingVisual() $ dveColors.vs
use render = dv.RenderOpen()
for i in 0..13 do
  for j in 0..2 do
    let y = 4 + (i / 7) * 68 + j * 20
    let x = 4 + (i % 7) * 28
    render.DrawRectangle(
      (getColor i j).br,
      null, 
      Rect(!.x, !.y, 20., 20.) )
)

(
let dv = DrawingVisual() $ dveColors.vs 
grid.hover =+ {
  let x, y = getCoordinate (poMouseOn grid)
  use render = dv.RenderOpen()
  render.DrawRectangle(
    null, 
    Pen("ccc".br, 4.), 
    Rect(x *. 28. + 2., y *. 68. + 2., 24., 64.) ) }
grid.mleave =+ {
  dv.RenderOpen().Close()
}
)

(
let dv = DrawingVisual() $ dveColors.vs 
let drawSelection i =
  use render = dv.RenderOpen()
  render.DrawRectangle(
    null, 
    Pen("0f0".br, 4.), 
    Rect(i % 7 *. 28. + 2., i / 7 *. 68. + 2., 24., 64.) )
drawSelection iColor.v
iColor => drawSelection
)

grid.mdown =+ {
  let x, y = getCoordinate (poMouseOn grid)
  iColor <*- (x + y * 7)
  }

