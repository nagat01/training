﻿module Deskel.Deskel
open FStap open __ open System.Windows.Controls
open System.Windows.Shapes
open Selector


let getColor i = getColor iColor.v i

let strength n (i:i) =
  match n with
  | LT 4 -> 2
  | LT 7 ->
    if i % 2 = 0 then 2 else 1
  | _ ->
    if   i % 4 = 0 then 2
    elif i % 2 = 0 then 1
    else 0

let stroke n i =
  let i = strength n i
  let w = (1.0 + 4. / !.(n+3)) * (pown 2. (i - 2))
  let co = getColor i
  stroke w co

let isLetter n i =
  match strength n (i+1) with
  | 2 -> true
  | 1 -> n <= 12
  | _ -> false

let szLetter w n =
  let n = [0..n-2].filter(fun i -> isLetter n i).len
  within 7. 18. (7. + 0.07 * w / n.f)

let letter (l:i) n i sz =
  let i = strength n (i+1)
  let co = getColor i
  let s = l.c.s
  BorderedText(s, co, sz) $ opa 0.7 $ zIdx 1

let clear (grid:Grid) =
  grid.clear
  grid.ColumnDefinitions.Clear()
  grid.RowDefinitions.Clear()

let draw (grid:Grid) =
  let w = grid.rw
  let h = grid.rh
  let c = hDeskel.v 
  let r = vDeskel.v 
  if btn.v then
    for _ in 0 .. c - 1 do grid.ColumnDefinitions <+ ColumnDefinition()
    for _ in 0 .. r - 1 do grid.RowDefinitions <+ RowDefinition()
    for i in 1 .. c - 1 do 
      Line() $ grid.addTo(col=i, rspan=r) $ xy2 0 0 0 2000 $ stroke c i |> nohit
    for i in 1 .. r - 1 do 
      Line() $ grid.addTo(row=i, cspan=c) $ xy2 0 0 2000 0 $ stroke r i |> nohit
  if btn.v && btnNumberAlphabet.v then
    let sz = szLetter w c
    for r in [0; r - 1] do
      let mutable l = 'A'.i - 1
      for i in 0 .. c - 2 do
        if isLetter c i then
          let l = &l +=! 1
          letter l c i sz
          $ If (c<=6) 
            (grid.addTo(col=i,row=r,rspan=2)) 
            (grid.addTo(col=i-1,row=max 0 (r-1),cspan=2,rspan=2))
          $ (If (r = 0) vaTop vaBottom) $ haRight |> nohit
    let sz = szLetter h r
    for c in [0; c - 1] do
      let mutable l = '0'.i
      for i in 0 .. r - 2 do
        if isLetter r i then
          let l = &l +=! 1
          letter l r i sz
          $ If (r<=6)
            (grid.addTo(row=i,col=c,cspan=2))
            (grid.addTo(row=i-1,col=max 0 (c-1),cspan=2,rspan=2)) 
          $ (If (c = 0) haLeft haRight) $ vaBottom |> nohit

let drawDeskel() =
  if btnIsRelative.v then
    draw deskelRel1
    draw deskelRel2 
  else
    draw deskel1
    draw deskel2

let eDeskelVis =
  ( nbHLine >< nbVLine >< iColor >< btnNumberAlphabet >< btnIsRelative) 
    -% { btn.v <- true; V true } >=< btn


let ini(pa:Panel) = 
  eDeskelVis =+ {
    clear deskel1
    clear deskel2
    clear deskelRel1
    clear deskelRel2
    drawDeskel() } 
  drawDeskel()

  Deskel.Selector.ini pa

