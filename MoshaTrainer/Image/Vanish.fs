﻿module Image.Vanish
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Media.Animation 
open Algo


let idBlur = ref 0
let iVanishType = pref.shI "iVanishType" 0

let fade st en dur (__ :FE) =
  let da = DoubleAnimation( From=Nullable !.st, To= Nullable !.en, Duration = Duration(sec dur) )
  let sb = Storyboard()
  sb.Children.Add da
  Storyboard.SetTargetProperty(da, PropertyPath(UIE.OpacityProperty) )
  sb.Begin __

let fadeImage en dur =
  let cur = gridImg3.Opacity
  let change = dist cur !.en
  let dur = if change = 0.0 then 0.0 else (0.1 + dur * 1.9) * change
  gridImg3 |> fade cur en dur

let slider key desc =
  let sp  = HStackPanel() 
  let lbl = Label' desc $ sp $ w 100 $ fsz 9 $ "fff".bg
  let sli = GageSlider(key, 0.5, 50., "ddd", "777", "777") $ sp
  let drawLabel() = lbl <*- sf "%s(%.2f)" desc (0.1 + sli.v *. 1.9) 
  drawLabel()
  sli =+ { drawLabel() }
  sp, sli


let cnv    = Canvas()
let  btn   = IconButton(Bitmap.vanish, key= "isVanish", coon = "ccc", opaoff= 0.25) 
let  sp    = StackPanel() $ "fff".bg
let  lbx   = ListBox() 
let   lbl0 = Label' "描いている時"            
let   lbl1 = Label' "マウスがキャンバス上の時" 
let   sp0, sliFadeOut = slider "imgFadeOutDur" "フェードアウト"
let   sp1, sliFadeIn  = slider "imagefadeindur" "フェードイン"

let ini (pa:Panel) =
  cnv |>* pa
  btn |>* tipIconButton "画像消滅" "あり" "なし" "" (sf "消滅ボタン:%s一定の条件の時に参考画像を消滅させる/させないを指定できます。")
  sp  |>* composeMenu cnv btn
  lbx |>* sp
  lbl0 $ lbx |>* tip "描いている時に参考画像を消去します。"
  lbl1 $ lbx |>* tip "マウスがキャンバス上の時、参考画像を消去します。"
  sp0 |>* sp
  sp1 |>* sp

let renew() = 
  idBlur := !idBlur + 1
  let id = !idBlur
  imgBlurred.Source <- null
  imgBlurred.Vis true
  tryImmediate' {
  do! 200
  do! (id = !idBlur)
  do! imgPath.v.ok
  let bmp = bimgSafe imgPath.v
  do! bmp.ok && id = !idBlur 
  let w, h, ps, bpp = bmp.pw, bmp.ph, bmp.pixelsI, bmp.Format.BitsPerPixel
  do! 1
  do! (id = !idBlur)
  match bpp with
    | 32 | 8 -> 
      let! ps =
        if bpp = 32
        then Blur32.Blur(w, h, ps, 1, idBlur, id).calc
        else Blur8.Blur(w, h, ps, 1, idBlur, id).calc
      do! (id = !idBlur)
      do! 1
      let wb = mkWriteableBitmap w h
      wb.writeAll ps
      imgBlurred <*- wb
    | _ -> ()
  }


lbx.idx <- iVanishType.v
lbx =+ { iVanishType <*- lbx.idx }

let filter i (__:_ obs) = __ -? { yield btn.v && lbx.idx = i }
filter 1 gridCnv.menter >< 
filter 0 gridCnv2.pmdown =+ { fadeImage 0 sliFadeOut.v.v }
filter 1 gridCnv.mleave >< 
filter 0 (wnd.mup >< dpSep.mup >< gridCnv2.mleave) =+ { fadeImage 1 sliFadeIn.v.v }

imgPath =+! {
  if btn.v then
    do! 50
    renew() }

btn => function
  | true -> 
    renew()
  | _ ->
    imgBlurred.Vis false

wnd.Loaded =+ { if btn.v then renew() } 

