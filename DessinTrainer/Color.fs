﻿module Color
open FStap open __ open System open System.Windows open System.Windows.Controls 
open System.Windows.Shapes
open System.Windows.Media
open System.Windows.Media.Imaging

let r = R32(0, 0, 255, 32)
let coC = sh "fff".co
let coA = sh "fff".co

type Category = 
  RGB | HSV
  override __.ToString() = match __ with RGB -> "RGB" | HSV -> "HSV"
type CiBtns = CiBtn<Category>
let cur<'__> = CiBtns.cur


type CoSli (r:R32, co:Co sh, ttl:s, get, set, map, fmt,?v:f ref) as __ =
  inherit HSpnl()
  let v = v |? ref 0.
  let _w, _h = r.w', r.h'
  let _    = Lbl ttl $ __
  let ca   = Canvas() $ __ $ size r.sz $ mgn2 (_h/2.) (_h/3.) 
  let  img = Image() $ w _w $ ca
  let   wb = mkWriteableBitmap r.w r.h $ img
  let  rA  = Rectangle() $ wh _h _h $ ca
  let  rC  = Rectangle() $ stroke 2 "0f0" $ wh _h _h $ ca
  let  cC  = ball 1. "000" 35. 35. $ tr (po (r.h'/2.)(r.h'/2.)) $ zIdx -1 $ ca
  let  lbl = Lbl "" $ __
  let renew (co:Co) =
    rA $ fill co $ stroke 2. co.inv |> mv (!v*_w-_h/2.) 0.
    wb.writeRectXY r (fun x _ -> (map(x.f/_w):Co).i)
    lbl <*- fmt !v
  do
    ca.lDrag.map(fun {Vec=vec} -> !v + vec.X / _w) >=< 
    ca.lmdown.map(fun e -> e.posOn(img).X / _w) => fun p -> 
      v := within 0. 1. p
      co <*- set !v
    co => renew
  member __.hide =
    v := get co.v
    hidden cC; hidden rC
  member __.show co =
    let show (__:UIE) = __ $ mv (get coC.v * r.w' - r.h' / 2.) 0. |> vis
    show rC
    cC $ fill ("c"+co) $ stroke 1 co |> show

type RgbSlider (r32:R32, co:Co sh) as __ =
  inherit Spnl()
  let mk ttl (get:_->y) set = 
    let get co = (get co).f / 255.
    let set f = (set (f*255.).y' co.v)
    CoSli(r, co, ttl, get, set, set, (fun v -> sf "%.0f" (v*255.))) $ __
  let sliR = mk "R" Co.R Co.setR 
  let sliG = mk "G" Co.G Co.setG
  let sliB = mk "B" Co.B Co.setB
  member __.co = co
  member __.hideC = vis __; sliR.hide; sliG.hide; sliB.hide;
  member __.showC r g b = sliR.show r; sliG.show g; sliB.show b

type HsvSlider (r:R32, co:Co sh) as __ =
  inherit Spnl()
  let mk ttl v get set map fmt = CoSli(r, co, ttl, get, set, map, fmt, v) $ __
  let h, s, v = ref 0., ref 0., ref 0.
  let f = Co.ofHsv
  let mapH x = f x 1. 1.
  let setH x = f x !s !v
  let setS x = f !h x !v
  let setV x = f !h !s x
  let sliH = mk "H" h Co.h setH mapH (_1 * 6. >> sf "%.2f")
  let sliS = mk "S" s Co.s setS setS (sf "%.2f")
  let sliV = mk "V" v Co.v setV setV (sf "%.2f")
  member __.co = co
  member __.hideC = vis __; sliH.hide; sliS.hide; sliV.hide
  member __.showC h s v = sliH.show h; sliS.show s; sliV.show v

let ca = mkCa
let laP = Lbl "" $ wh 200 200 $ ca
let laA = Lbl "" $ wh 200 200 $ mv 200 0 $ ca
let rgbSlider = RgbSlider (r, coA) $ mv 25 210 $ ca
let hsvSlider = HsvSlider (r, coA) $ mv 25 210 $ ca

coA => fun co -> co.bg laA
coC => fun co -> co.bg laP

ca.lclick -? { Not timer.enabled } =+ {
  collapse ca
  timer.start
  if nextCate then CiBtns.rand
  coC <*- Co.rand
  coA <*- Co.rand
  match cur with RGB -> rgbSlider.hideC | HSV -> hsvSlider.hideC
  lblMsg <*- sf "正しい%s値の色を作成してください。右クリックすると採点されます。" cur.s
  coA.ini
  [laP; laA] |%| vis }


ca.MouseRightButtonDown -? { is timer.enabled } =+ {
  timer.stop
  let er f = abs(f coC.v - f coA.v)
  let sc cate f = scoring cate 5. 120. (er f)
  match cur with 
  | RGB -> 
    let f s f = sc s (f>>float)
    rgbSlider.showC (f "R" Co.r) (f "G" Co.g) (f "B" Co.b)
  | HSV ->
    let f s f = sc s (f>>(*) r.w')
    let h = scoring "H" 10. 40. (min (er Co.h) (6. - er Co.h) * r.w')
    hsvSlider.showC h (f "S" Co.s) (f "V" Co.v) }

let ini<'__> = iniTabi CiBtns.all "カラー" ca
