﻿module Geometric
open FStap open __ open System open System.Windows open System.Windows.Controls 
open System.Windows.Shapes
open System.Windows.Media

let walk = walk 30. 40.

type Category = 
  Intersect | RightAngle | Circle | Bisect | MidPo | Parallelogram
  override __.ToString() = match __ with Intersect -> "２線の交点" | RightAngle -> "直角な線" | Circle -> "円の中心" | Bisect -> "二等分線" | MidPo -> "２点の中点" | Parallelogram -> "平行四辺形"
type CiBtns = CiBtn<Category>
let ca  = mkCa
let circleAux= circle    "ccc"         $ ca
let circleP  = circle    "000"         $ ca
let circleC  = ball   1. "000" 50. 50. $ ca
let cross0   = cross 10. "000"         $ ca
let cross1   = cross 10. "000"         $ ca
let polyP    = polyline  "000"         $ ca
let polyA    = polyline  "44f"         $ ca
let polyC    = polyline  "0f0"         $ ca
let leverA   = lever     "44f"         $ ca
let leverC   = lever     "0f0"         $ ca

collapse ca

let showAnswer a b c (d:Po) = visMany [polyA$pnt3 a b c; leverA$d.mv]

let setCollect a b c (d:Po) = pnt3 a b c polyC; d.mv leverC

ca.lclick -? { Not timer.enabled } => fun _ ->
  timer.start
  collapse ca
  if nextCate then CiBtns.rand
  let next = ref true 
  while !next do
  let a,b,c = rpo,rpo,rpo 
  match CiBtns.cur with
  | Intersect ->
    print "２つの直線の交点に合わせてください"
    let d = walk b
    if far3 half a b c && isInside d then
      let a1 = arrow "000" a b $ ca
      let a2 = arrow "000" c b $ ca
      showAnswer a d c d
      hidden polyA
      setCollect a b c b 
      next := false
  | RightAngle ->
    print "直角になる直線を作ってください"
    let l = (a-b).len
    let v = rotate 90. l (a-b)
    let c = b + v
    let d = walk c
    if far half a b && isInsides [c;d] then
      polyP $ pnt3 a b b |> vis
      circleAux $ (b - vec l l).mv $ size(sz l l *. 2.) |> vis
      showAnswer b d b d
      setCollect b c b c
      next := false
  | Circle ->
    print "円の中心を当ててください"
    let sz = sz (r half xma) (r half yma)
    let b = a +. sz /. 2.
    let c = walk b 
    if isInside c then
      circleP $ a.mv $ size sz |> vis
      showAnswer c c c c
      hidden polyA
      setCollect b b b b
      next := false
  | Bisect ->
    print "角度を２等分する直線を作ってください"
    let ab, cb = a - b, c - b 
    let a' = b + ab / 2.
    let c' = b + cb / 2.
    let d = (a *. cb.len +. c *. ab.len) /. (ab.len + cb.len)
    let r = d.dist b 
    let e = walk d
    if far3 half a b c && isInside e then
      polyP $ pnt3 a' b c' |> vis
      circleAux $ (b - vec r r).mv $ wh (r*2.) (r*2.) |> vis
      showAnswer b e b e
      setCollect b d b d
      next := false 
  | MidPo ->
    print "２つの点の真ん中の点をあててください"
    let c = (a +. b) /. 2.
    let d = walk c    
    if far 250. a b && isInside d then
      cross0 $ a.mv |> vis
      cross1 $ b.mv |> vis
      showAnswer a d b d
      hidden polyA
      setCollect a c b c
      next := false
  | Parallelogram ->
    print "平行四辺形を作ってください"
    let d = a +. c -. b
    let e = walk d 
    if far3 100. a b c && far 100. b d && isInsides [d;e] then
      polyP $ pnt3 a b c |> vis
      showAnswer a e c e
      setCollect a d c d
      next := false

leverA.lDragAbs -? { is timer.enabled } => fun {Vec=v} -> 
  polyA.Points.[1] <- polyA.Points.[1] + v
  v.mv leverA

leverA.mup -? { is timer.enabled } => fun e -> 
  e.Handled<-true
  visMany [polyC; leverC]
  let co = scoring CiBtns.cur.s 6. 10. (leverC.po.dist leverA.po)
  circleC $ coCircle co $ leverC.po.mv |> vis 
  timer.stop

let ini<'__> = iniTabi CiBtns.all "図形" ca
