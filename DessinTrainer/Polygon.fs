﻿module Polygon
open FStap open __ open System.Windows.Controls 
open System.Windows.Shapes

type Category = 
  LR3 | LR4 | Rot3 | Rot4 | Def3 | Def4 | Inf3 | Inf4
  member __.sym = match __ with LR3  | LR4  | Rot3 | Rot4 -> true | _ -> false
  member __.lr  = match __ with LR3  | LR4                -> true | _ -> false
  member __.def = match __ with Def3 | Def4               -> true | _ -> false
  member __.nv  = match __ with LR3  | Rot3 | Def3 | Inf3 -> 3    | _ -> 4
  override __.ToString() = 
    match __ with 
    | LR3  | LR4  -> "左右対称" 
    | Rot3 | Rot4 -> "点対称" 
    | Def3 | Def4 -> "縮小" 
    | _           -> "拡大"
    |> _1 + __.nv.s

type CiBtns = CiBtn<Category>
let cur<'__> = CiBtns.cur
let nv<'__>  = cur.nv

let ca = mkCa
// let levers co n = array n ((function 0 -> "000" | _ -> co) >> lever >>! ca.add)
let levers co n = [| for i in 0 .. n-1 -> (match i with 0 -> "000" | _ -> co) |> lever $ ca |]
let poly (co:s) = Polygon() $ stroke 2 co $ rounded $ ca

let poVanish = Canvas() $ ca $ cs [ball 1. "000" 8. 8.; ball 1. "000" 1. 1.]  
let sp       = Line()   $ ca $ stroke 1 "000" $ xy2 half 0 half hca 
let circlesC = array' 5 { yield ball 1. "000" 50. 50. $ ca }
let linesAux = array' 5 { yield line 2. "2000"        $ ca }
let polyP    = poly   "000"
let polyA    = poly   "77f"
let polyC    = poly   "0f0"
let leversP  = levers "000" 5
let leversA  = levers "77f" 5
let leversC  = levers "0f0" 5

collapse ca

let reflect (p:Po) = (po (wca - p.X) p.Y)
let rotate  (p:Po) = (po (wca - p.X) (hca - p.Y))
let rpo<'__> = if cur.sym then (if cur.lr then rLeftPo else rTopPo) else rpo
let move p   = cur.lr.If reflect rotate p
let shrink sc (vpo:Po) (p:Po)       = p *. sc +. vpo *. (1. - sc)
let leversPnt (ps:Po[]) (__:Grid[]) = __ |%| mv -100 -100; for p,g in Seq.zip ps __ do  p.mv g

let mutable ps, as', (cs:Po[]), vpo = Def, Def, Def, Def
ca.lclick -? { Not timer.enabled } => fun e ->
  e.Handled <- true
  if nextCate then CiBtns.rand
  if cur.sym then 
    if cur.lr then vis sp; "左右対称" else "点対称"
    |> _1 + (sf "な%d角形を作ってください。右クリックすると、採点されます。" nv) |> print
  else print (sf "相似な %d 角形を作ってください。右クリックすると、採点されます。" nv)
  let next = ref true
  let f () =
    as' <- cs.[0] +++ (cs.[1..] |%> walk 15. 20.)
    next := isInsides(as').n
  while !next do
    vpo <- cur.sym.If (po half half) rpo
    ps <- array nv (fun _ -> rpo)
    if isConvex ps then
      if cur.sym then
        if farn (hca / (1.0 + 0.5 * nv.f)) ps then
          cs <- ps |%> move
          f ()
      elif (ps |%> far (wca/2.5) vpo).forall id && farn (wca / nv.f) ps then
        let ss = ps |%> shrink (Rng.f 0.5 0.8) vpo
        cs <- cur.def.If ss ps
        ps <- cur.def.If ps ss
        f ()

  collapse ca
  timer.start
  if cur.lr then vis sp
  else
    poVanish $ vpo.mv |> vis
    for i in 0..nv-1 do
      let p0 = if cur.sym then vpo *. 10. -. ps.[i] *. 9. else vpo
      linesAux.[i] $ p0p1 p0 (ps.[i] *. 10. -. vpo *. 9.) |> vis
  polyP  $ pnts ps  |> vis
  polyA  $ pnts as' |> vis
  polyC  |> pnts cs
  leversP $ leversPnt ps  |%| vis
  leversA $ leversPnt as' |%| vis
  leversC |> leversPnt cs
  for i in 1..nv-1 do cs.[i].mv circlesC.[i]


for n in 1..4 do
  leversA.[n].lDragAbs -? { is timer.enabled } => fun {Vec=v} ->
    v.mv leversA.[n]
    polyA.Points.[n] <- polyA.Points.[n] + v

ca.MouseRightButtonDown.[fun e -> timer.enabled] =+ {
  polyC |> vis
  leversC |%| vis          
  let dist n = leversA.[n].po.dist leversC.[n].po
  for n in 1..nv-1 do
    let co = scoring (sf "%s 点%d" cur.s (n+1)) 4. ((nv-1).f * 10.) (dist n)
    circlesC.[n] $ coCircle co |> vis
  timer.stop }

let ini<'__> = iniTabi CiBtns.all "対象と相似" ca
