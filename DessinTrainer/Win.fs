﻿[<AutoOpen>]
module __.Win
open FStap open System open System.Windows open System.Windows.Controls

let mutable problems, solved, solvedStat, score, scoreStat = 0, 0, 0, 0., 0.

let wnd         = Window'"DessinTrainer" $ szToCtt
let dp        = Dpnl() $ "f7f7ff".bg $ wnd
let  sp       = HSpnl() $ "fff".bg $ dp.top
let   timer     = TimerLbl() $ sp
let   tbtnTimer = TBtn ("時間で減点",on="f77", off="ddf", v=true) $ sp
let   nudCate   = Nud ([1;2;3;4;5;6;8;10;15;20], trail=Lbl "問で次の種類", idx=2) $ sp
let   lblRound  = Lbl "１ラウンド" $ sp
let   nudRound  = Nud ([10;20;30;50;100;150;200], trail=Lbl "問", idx=1) $ sp
let   lblResult = Lbl "" $ sp
let   btnClear  = Button'("クリア","fff") $ sp
let  spCases  = HSpnl() $ dp.top
let  dp1      = Dpnl()  $ dp.top
let   tabc      = TabControl() $ pad1 0 $ bdr 0 "0fff" $ dp1.left
let   lbxSolves = ListBox() $ "fff".bg $ wh 250 250 $ dp1.top
let   lbxStats  = ListBox() $ "fff".bg $ w 250 $ dp1.top
let  lblMsg     = Lbl "" $ "efe".bg $ "400".fg $ dp.bottom

btnClear =+ {
  solved     <- 0
  solvedStat <- 0
  score      <- 0.
  scoreStat  <- 0.
  lbxSolves.clear
  lbxStats.clear
  lblResult <*- "" }

tabc =+ { lblMsg <*- "キャンバスをクリックすると問題がはじまります。" }

let nextCate<'__> = incM &problems % nudCate.v = 1 && problems <> 1 

let iniTabi (cases:'a CiBtn seq) hdr (ca:Canvas) =
  tabc.addHdr hdr ca
  tabc.cur hdr =+ { 
    collapse ca
    timer.clear
    spCases.clear
    cases |%| spCases.add }

let scToCo sc = match sc with | GT 100. -> "ff8" | GT 80. -> "9f9" | GT 60. -> "cdf" | GT 40. -> "b8d" | GT 20. -> "aa9" | _ -> "f77"
let scoring category deduct lim err =
  lblMsg <*- "キャンバスをクリックすると、次の問題になります。"
  let tsc = if tbtnTimer.v then (lim * 1.2 - timer.v.tsec) / lim else 1.
  let sc = max 1. ((105. - (err * deduct) / sqrt (1. + (err / (150. / deduct)) ** 2.)) * tsc)
  incm &solved
  incm &solvedStat
  addm sc &score
  addm sc &scoreStat
  let lbl s = Lbl s $ pad2 2 1 $ equalWidth
  let lblSc (sc:f) = lbl (sf "%d点" sc.i) $ padw 10 $ (scToCo sc).co.setA(127uy).bg
  let lblStat (solved:i) sc = HSpnl() $ cs [lbl (sf "解いた数: %d問  合計: %.0f点  平均: " solved sc); lblSc (sc/solved.f)] $ "fff".bg $ haStretch
  HSpnl() $ cs [lbl (sf "%d. %s %2.0fpx %s秒" solvedStat category err timer.ctt); lblSc sc] $ haStretch $ lbxSolves |> lbxSolves.ScrollIntoView
  lblResult <*- lblStat solved score 
  if solvedStat = nudRound.v then
    let mk () = lblStat solvedStat scoreStat 
    mk () $ lbxStats |> lbxStats.ScrollIntoView
    let lv = ["ピカソ"; "絵歴５０年レベル"; "有名美術家"; "美大生"; "美大受験予備校で沢山浪人してる人"; "絵歴３年"; "美術部部員"; "美術部体験入部"; "クラスの絵が上手いやつ"; "中学生"; "小学生"; "幼稚園児"; "赤ちゃん"].rev.[(scoreStat.i / solvedStat) / 8]
    let sp = Spnl () $ cs [lbl (sf "あなたのデッサン力は %s レベルです" lv) $ mgn1 10.; mk() $ mgn1 10.]
    let w = 
      Window(Owner=wnd, WindowStyle=WindowStyle.ToolWindow, WindowStartupLocation=WindowStartupLocation.CenterOwner) 
      $ ttl (sf "%d回分の結果が出ました！" solvedStat) $ ctt sp $ fsz 20. $ szToCtt
    w.Show()
    solvedStat <- 0; scoreStat <- 0.
  scToCo sc

let print s = lblMsg <*- sf "レバーをドラッグして、%s" s

let ini = wnd |> ignore