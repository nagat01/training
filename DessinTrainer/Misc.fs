﻿[<AutoOpen>]
module __.Misc
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes
open System.Windows.Media


type TabControl with 
  member __.cur hdr = 
    __ -??  fun e -> (e.AddedItems.[0] :?> TabItem).Header = hdr

let coCircle (co:s) (__:Shape) = 
  __ $ fill (co.co.setA 64uy) |> stroke 1 co 

let collapse (ca:Canvas) = ca |%| collapsed


type TimerLbl () as __ =
  inherit Lbl "0.0"
  do __ $ bold $ w 50 $ hcaRight |> "000".bg
  let sw = Timer(Interval=msec 100)
  let mutable elapsed = Zero
  do sw =+ { __ <*- (addM (msec 100) &elapsed).S "s'.'f" }
  member __.v       = elapsed
  member __.ctt     = elapsed.S "s'.'f"
  member __.start   = sw.Start(); "f40".fg __; elapsed <- Zero
  member __.stop    = sw.Stop (); "7cf".fg __
  member __.clear   = sw.Stop (); __ <*- "" 
  member __.enabled = sw.IsEnabled


type CiBtn<'T when 'T: equality>(ci) as __ = 
  inherit TBtn(ci.s, bgon="ffc", bgoff="fff", v=true)
  static let _all = [ for c in cases<'T> -> CiBtn c]
  static let mutable _cur = Def
  static do CiBtn<'T>.cur <- _all.[0].ci
  do __.lclick =+ { if _all.[fun c -> c.v].len < 2 then __.v <- false }
  member __.ci = ci 
  static member cur 
    with set x = _cur <- x; for c in _all do c.v <- c.ci = _cur
    and get () = _cur
  static member all = _all
  static member rand =
    let cs = _all.fmap(fun c -> c.v, c.ci)
    CiBtn<'T>.cur <- match cs.findIdx ((=)CiBtn<'T>.cur) with Some i -> cs.[(i+1)%cs.len] | None -> cs.[0]


// math
let rotate ang len (v:Vec) =
  let m = Matrix.Identity
  m.Rotate ang
  let sc = len / v.Length
  m.Scale (sc, sc)
  m.Transform v

let wca, hca = 400., 400.
let szca __  = size (sz wca hca) __
let half     = hca/2.
let r a b    = Rng.f a b
let r2 a b   = r a b * Rng.b.If 1. -1.
let xma<'__> = wca  - 25.
let yma<'__> = wca  - 25.
let hma<'__> = half - 25.
let rpo<'__>     = po (r 25. xma) (r 25. yma)
let rLeftPo<'__> = po (r 25. hma) (r 25. yma)
let rTopPo<'__>  = po (r 25. xma) (r 25. hma)
let walk mi ma (p:Po)     = p + vec (r2 mi ma) (r2 mi ma)
let isInside  (__:Po)     = isInside 25. 25. xma yma __
let isInsides (__:Po seq) = __.forall isInside


// wpf
let mkCa<'__> = Canvas() $ szca $ "fff".bg

 
// shapes
let line th (co:s)        = Line()     $ stroke th co
let cross w co            = Grid()     $ cs [line 1. co $ xy2 -w 0 w 0 ; line 1. co $ xy2 0 -w 0 w]
let circle  (co:s)        = Ellipse()  $ stroke 2 co 
let ball th (co:s) w h    = Ellipse()  $ stroke th co $ fill ("2"+co) $ tr (po -w -h) $ wh (w*2.) (h*2.)
let lever co              = Grid()     $ cs [cross 8. co; ball 1. co 14. 14.]
let polyline (co:s)       = Polyline() $ stroke 2 co $ rounded $ collapsed
let arrow (co:s) p (q:Po) =
  let q = p + rotate 0. 120. (q-p) 
  let ln ang len = Line() $ p0p1 (p + rotate ang len (p-q)) p $ stroke 2 co
  Grid() $ cs [ln 0. 120.; ln 30. 10.; ln -30. 10.] 

